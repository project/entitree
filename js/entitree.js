(function ($, Drupal, once) {
  Drupal.behaviors.entitree = {
    attach: function (context, settings) {
      // Apply events using once()
      // @see https://www.drupal.org/docs/drupal-apis/javascript-api/javascript-api-overview
      once('entitree-menu-toggle-enabled', '.entitree-location-operations-icon', context).forEach(function (element) {
        $(element, context).on('click', onOperationsIconClick);
      });
      once('entitree-menu-toggle-enabled', '.entitree--tree-display .location, .entitree-locations-location .location', context).forEach(function (element) {
        $(element, context).on('mouseleave', onLocationMouseExit);
      });
    },
  };

  var onOperationsIconClick = function(){
    var $location = $(this).closest('.location');
    if($location.hasClass('expanded-operations')){
      collapseLocationOperations($location);
    }
    else{
      expandLocationOperations($location);
    }
  };

  var onLocationMouseExit = function(){
    var $location = $(this);
    if ($location.hasClass('expanded-operations')) {
      collapseLocationOperations($location);
    }
  };

  var expandLocationOperations = function($location){
    $location.addClass('expanded-operations');
  };

  var collapseLocationOperations = function($location){
    $location.removeClass('expanded-operations');
  };
})(jQuery, Drupal, once);
