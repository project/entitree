<?php

namespace Drupal\entitree\Routing;

/**
 * Defines dynamic routes.
 */
class DynamicEntitreeRoutes {

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $routes = [];

    $entitreeTypesManager = \Drupal::service('plugin.manager.entitree_entity_type');

    foreach ($entitreeTypesManager->getDefinitions() as $typeDefinition) {
      // Foreach enabled type plugin get an instance of the class.
      $plugin = $entitreeTypesManager->getEntitreeEntityType($typeDefinition['id']);

      $plugin->buildManageLocationRoute($routes);
    }

    return $routes;
  }

}
