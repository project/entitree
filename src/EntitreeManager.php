<?php

namespace Drupal\entitree;

use Drupal\mysql\Driver\Database\mysql\Connection;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\entitree\Entity\EntitreeLocation;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entitree\Event\EntitreeOperationsEvent;

/**
 * Service of utility methods to manage Entitree locations.
 */
class EntitreeManager implements EntitreeManagerInterface {
  use StringTranslationTrait;

  /**
   * Drupal\mysql\Driver\Database\mysql\Connection definition.
   *
   * @var \Drupal\mysql\Driver\Database\mysql\Connection
   */
  protected $database;

  /**
   * Stores instances of nested set factories keyed by langcode.
   *
   * @var array
   */
  protected $nestedSetFactories = [];

  /**
   * Stores instance of nested set factory
   *
   * @var NestedSetFactory
   */
  protected $nestedSetFactory;

  /**
   * Entitree.config factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Entitree config object.
   *
   * @var Drupal\Core\Config\ImmutableConfig
   */
  protected $entitreeConfig;

  /**
   * Array of EntitreeStore instances keyed by langcode.
   *
   * @var array
   */
  protected $stores = [];

  /**
   * Associative array of UUID => EntitreeLocation pairings.
   *
   * Contains all locations already loaded from the database as a flat array.
   *
   * @var array
   */
  protected $loadedLocations = [];

  /**
   * Constructs a new EntitreeManager object.
   */
  public function __construct(Connection $database, ConfigFactoryInterface $config_factory) {
    $this->database = $database;
    $this->configFactory = $config_factory->getEditable("entitree.config");
    $this->entitreeConfig = \Drupal::config("entitree.config");

    // Prepare nested set library for use.
    $this->nestedSetFactory = new NestedSetFactory(\Drupal::languageManager()->getCurrentLanguage()->getId());
  }

  /**
   * {@inheritDoc}
   */
  public function getNestedSetFactory(string $langcode): NestedSetFactory {
    if (!isset($this->nestedSetFactories[$langcode])) {
      $this->nestedSetFactories[$langcode] = new NestedSetFactory($langcode);
    }

    return $this->nestedSetFactories[$langcode];
  }

  /**
   * {@inheritDoc}
   */
  public function getStore(string $langcode): EntitreeStoreInterface {
    if (!isset($this->stores[$langcode])) {
      $this->stores[$langcode] = new EntitreeStore($langcode, $this);
    }

    return $this->stores[$langcode];
  }

  /**
   * {@inheritDoc}
   */
  public function getConfig() {
    return $this->entitreeConfig;
  }

  /**
   * {@inheritDoc}
   */
  public function updateConfig(string $name, $value) {
    $this->configFactory->set($name, $value)->save();
  }

  /**
   * {@inheritDoc}
   */
  public function getLocationsByIdAndType(int $entityId, string $entityType): ?array {
    $query = \Drupal::database()->select('entitree_location', 'el');
    $query->condition('el.ref_entity_id', $entityId);
    $query->condition('el.ref_entity_type', $entityType);
    $query->fields('el', ['id', 'langcode']);
    $result = $query->execute();
    $locationRows = $result->fetchAllAssoc('id');

    $locations = [];

    foreach ($locationRows as $id => $row) {
      $locations[] = $this->getStore($row->langcode)->getLocation($id);
    }

    return $locations;
  }

  /**
   * {@inheritDoc}
   */
  public function getLocationByPath(string $path): ?EntitreeLocationInterface {
    // Find the path and its corresponding entitree location.
    $database = \Drupal::database();
    $query = $database->query("SELECT l.id from entitree_location l, path_alias p WHERE p.alias = :path AND p.id = l.pid", [
      ':path' => $path,
    ]);
    $result = $query->fetchAssoc();

    if ($result) {
      return EntitreeLocation::load($result['id']);
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getMainLocation(EntityInterface $entity): ?EntitreeLocationInterface {
    $query = \Drupal::database()->select('entitree_location', 'el');
    $query->condition('el.ref_entity_id', $entity->id());
    $query->condition('el.ref_entity_type', $entity->getEntityTypeId());
    $query->condition('el.langcode', $entity->language()->getId());
    $query->fields('el', ['id']);
    $query->range(0, 1);
    $result = $query->execute();
    $result = $result->fetchObject();

    if ($result) {
      return $this->getStore($entity->language()->getId())->getLocation($result->id);
    }

    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getSiteRootLocation(string $langcode): EntitreeLocationInterface {
    $siteRootLocationId = \Drupal::state()->get('entitree.site_root_location');

    return EntitreeLocation::load($siteRootLocationId);
  }

  /**
   * {@inheritDoc}
   */
  public function getLangcodeByLocation(int $locationId): string {
    $location = EntitreeLocation::load($locationId);
    return $location->getLangcode();
  }

  /**
   * {@inheritDoc}
   */
  public function generateEntityTypeOperations(array $types): array {
    $entitreeEntityTypeManager = \Drupal::service('plugin.manager.entitree_entity_type');

    $operations = [];

    foreach ($types as $type) {
      $plugin = $entitreeEntityTypeManager->getEntitreeEntityType($type);

      if (!$plugin) {
        throw new \Exception('The entity type ' . $type . ' does not have a corresponding plugin');
      }

      $initialOperations = $plugin->getOperations();

      /*
       * Invoke the entitree operations event in case other modules add to the
       * operations list.
       */
      $event = new EntitreeOperationsEvent($initialOperations);
      $eventDispatcher = \Drupal::service('event_dispatcher');
      $eventDispatcher->dispatch(EntitreeOperationsEvent::EVENT_NAME, $event);

      // Store operations for this type to config.
      $typeOperations = [
        'entity_type' => $type,
        'operations' => [],
      ];

      foreach ($event->getOperations() as $key => $values) {
        $typeOperations['operations'][] = array_merge(
          ['operation_name' => $key],
          $values
        );
      }

      $operations[] = $typeOperations;
    }

    $this->updateConfig('entity_type_operations', $operations);

    return $operations;
  }

  /**
   * {@inheritDoc}
   */
  public function getEntityTypeOperations(): array {
    $configArray = $this->entitreeConfig->get('entity_type_operations', []);

    $operations = [];

    foreach ($configArray as $configOperations) {

      // Restructure outgoing operations arrays to be keyed by operation name.
      $entityTypeOperations = [];
      foreach ($configOperations['operations'] as $configOperation) {
        $entityTypeOperations[$configOperation['operation_name']] = $configOperation;
      }

      $operations[$configOperations['entity_type']] = $entityTypeOperations;
    }

    return $operations;
  }

  /**
   * {@inheritDoc}
   */
  public function getAllSupportedTokenTypes(): array {
    $definitions = \Drupal::service('plugin.manager.entitree_entity_type')->getDefinitions();

    $tokenTypes = [];

    foreach ($definitions as $definition) {
      $tokenTypes = array_unique(array_merge($tokenTypes, call_user_func($definition['class'] . '::getTokenTypes')));
    }

    return $tokenTypes;
  }

  /**
   * {@inheritDoc}
   */
  public function applyEntityTokenReplaceAction(string $value, EntityInterface $entity): string {
    // Load the appropriate definition.
    $definitions = \Drupal::service('plugin.manager.entitree_entity_type')->getDefinitions();

    $entityType = $entity->getEntityTypeId();

    if (!isset($definitions[$entityType])) {
      throw new \Exception('The entity of type ' . $entityType . ' is not supported by the current Entitree configuration.');
    }

    return call_user_func($definitions[$entityType]['class'] . '::applyTokenReplaceActions', $value, $entity);
  }

  /**
   * {@inheritDoc}
   */
  public function generateLocationSelectionFormArray(string $wrapper, EntitreeLocationInterface $parent = NULL, string $pageHelp = ""):array {
    $out = [
      '#type' => 'fieldset',
      '#title' => $this->t('Select Entitree Location'),
      '#tree' => TRUE,
    ];

    /*
     * If a parent location was not provided in the path use the root location
     * as the parent.
     */
    if (!$parent) {
      $parent = $this->getSiteRootLocation(\Drupal::languageManager()->getCurrentLanguage()->getId());
    }

    // Get children to render for this parent.
    $children = $parent->getChildren();

    // Load the ancestors of the parent location.
    $ancestors = $parent->getAncestors();

    // Create ancestors list.
    $out['ancestors'] = [
      '#prefix' => '<div class="entitree--tree-display--column entitree--column--narrow">',
      '#markup' => '',
      '#suffix' => '</div>',
    ];

    foreach ($ancestors as $ancestor) {
      /*
       * @todo ideally the browse option would be a link element, but there is
       * currently a bug that breaks AJAX functionality on links
       * https://www.drupal.org/project/drupal/issues/2915954
       */
      $out['ancestors']['ancestor_' . $ancestor->id()] = [
        '#markup' => '',
        '#prefix' => '<div class="location location-ancestor">',
        '#suffix' => '</div>',
      ];
      $out['ancestors']['ancestor_' . $ancestor->id()]['ancestor_' . $ancestor->id() . '_browse'] = [
        '#type' => 'button',
        '#value' => $ancestor->getLabel(),
        '#name' => 'entitree_browser_location_browse_' . $ancestor->id(),
        '#ajax' => [
          'callback' => '::onEntitreeLocationBrowse',
          'wrapper' => $wrapper,
        ],
        '#data' => [
          'location_id' => $ancestor->id(),
          'method' => 'browse',
        ],
      ];
      $out['ancestors']['ancestor_' . $ancestor->id()]['ancestor_' . $ancestor->id() . '_select'] = [
        '#type' => 'button',
        '#value' => 'S',
        '#name' => 'entitree_browser_location_select_' . $ancestor->id(),
        '#ajax' => [
          'callback' => '::onEntitreeLocationSelect',
          'wrapper' => $wrapper,
        ],
        '#data' => [
          'location_id' => $ancestor->id(),
          'method' => 'select',
        ],
      ];
    }

    // Add the parent to the ancestors tree, but make it not a browsable option.
    $out['ancestors']['parent'] = [
      '#markup' => '',
      '#prefix' => '<div class="location location-ancestor location-parent">',
      '#suffix' => '</div>',
    ];
    $out['ancestors']['parent']['parent_browse'] = [
      '#markup' => $parent->getLabel(),
    ];
    $out['ancestors']['parent']['parent_select'] = [
      '#type' => 'button',
      '#value' => 'S',
      '#name' => 'entitree_browser_location_select_' . $parent->id(),
      '#ajax' => [
        'callback' => '::onEntitreeLocationSelect',
        'wrapper' => $wrapper,
      ],
      '#data' => [
        'location_id' => $parent->id(),
        'method' => 'select',
      ],
    ];

    // Create children list.
    $out['children'] = [
      '#prefix' => '<div class="entitree--tree-display--column entitree--column--wide">',
      '#suffix' => '</div>',
    ];

    foreach ($children as $child) {
      /*
       * @todo ideally this would be a link element, but there is currently a
       * bug that breaks AJAX functionality on links
       * https://www.drupal.org/project/drupal/issues/2915954
       */
      $out['children']['child_' . $child->id()] = [
        '#markup' => '',
        '#prefix' => '<div class="location location-child">',
        '#suffix' => '</div>',
      ];
      $out['children']['child_' . $child->id()]['child_' . $child->id() . '_browse'] = [
        '#type' => 'button',
        '#value' => $child->getLabel(),
        '#name' => 'entitree_browser_location_browse_' . $child->id(),
        '#ajax' => [
          'callback' => '::onEntitreeLocationBrowse',
          'wrapper' => $wrapper,
        ],
        '#data' => [
          'location_id' => $child->id(),
          'method' => 'browse',
        ],
      ];
      $out['children']['child_' . $child->id()]['child_' . $child->id() . '_select'] = [
        '#type' => 'button',
        '#value' => 'S',
        '#name' => 'entitree_browser_location_select_' . $child->id(),
        '#ajax' => [
          'callback' => '::onEntitreeLocationSelect',
          'wrapper' => $wrapper,
        ],
        '#data' => [
          'location_id' => $child->id(),
          'method' => 'select',
        ],
      ];
    }

    return $out;
  }

}
