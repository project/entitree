<?php

namespace Drupal\entitree\TwigExtension;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\entitree\EntitreeLocationInterface;
use Drupal\entitree\EntitreeManagerInterface;
use Drupal\Core\Url;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * A custom twig extension for Entitree.
 */
class EntitreeTwigExtension extends AbstractExtension {

  /**
   * The Drupal renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * The entitree manager service.
   *
   * @var \Drupal\entitree\EntitreeManagerInterface
   */
  protected $entitreeManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(RendererInterface $renderer, EntitreeManagerInterface $entitree_manager) {
    $this->renderer = $renderer;
    $this->entitreeManager = $entitree_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function getTokenParsers() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getNodeVisitors() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTests() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('entitree_location_operations',
        [$this, 'renderLocationOperations'],
        ['is_safe' => ['html']]
      ),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getOperators() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'entitree.twig.extension';
  }

  /**
   * Render the display of operations for an Entitree location.
   *
   * @param \Drupal\entitree\EntitreeLocationInterface $location
   *   The Entitree location.
   * @param array $destination
   *   An array of destination details for specific operations.
   *
   * @return \Drupal\Component\Render\MarkupInterface
   *   The resulting markup.
   */
  public function renderLocationOperations(EntitreeLocationInterface $location, array $destination = []): MarkupInterface {
    // TODO implement proper access check for operations.
    /*
     * If select is defined in destination list only show the select operation
     * and the location being rendered isn't the selection target show the
     * select option.
     */
    if (isset($destination['select'])) {
      /*
       * Only add the selet operation if this location isn't the location
       * looking for a parent.
       */
      if (isset($destination['select']['parameters']['location']) && $destination['select']['parameters']['location'] === $location->getId()) {
        $operations = [];
      }
      else {
        $operations = [
          'select' => [
            'title' => t('Select this Location'),
            'label' => 'Select',
            'url' => Url::fromRoute($destination['select']['route'], $destination['select']['parameters'] + ['parent' => $location->getId()]),
            'weight' => -50,
          ],
        ];
      }
    }
    else {
      $operations = \Drupal::entityTypeManager()->getListBuilder('entitree_location')->getOperations($location);

      // Remove delete_location operation on site root.
      if ($location->isSiteRoot()) {
        unset($operations['delete_location']);
      }
    }

    $elements = [
      [
        '#theme' => 'entitree__location_operations',
        '#operations' => $operations,
      ],
    ];
    return $this->renderer->render($elements);
  }

}
