<?php

namespace Drupal\entitree;

use Zend\Db\Sql\Predicate\Predicate;
use metalinspired\NestedSet\Config;

/**
 * Interface for the nested set factory.
 */
interface NestedSetFactoryInterface {

  /**
   * Constructor method for NestedSetFactory.
   *
   * @param string $langcode
   *   Two-letter langcode for the corresponding Entitree structure.
   */
  public function __construct(string $langcode);

  /**
   * Get the NestedSet config object containing database information.
   *
   * @return \metalinspired\NestedSet\Config
   *   The NestedSet Config instance.
   */
  public function getConfig(): Config;

  /**
   * Get the ID of the root node for this Entitree store.
   *
   * @return int
   *   The root node ID.
   */
  public function getRootNodeId(): int;

  /**
   * Create the root node for this Entitree store.
   */
  public function createRootNode();

  /**
   * Insert a node into the Entitree store.
   *
   * @param int $parent
   *   Node ID of the parent node.
   * @param array $data
   *   Data for the new node.
   */
  public function insert(int $parent, array $data = []);

  /**
   * Find all descendants of a node in the Entitree store.
   *
   * @param int $id
   *   The node ID of the ancestor.
   * @param array|null $columns
   *   Columns from the database to return for each node.
   * @param string|null $order
   *   Comma-separated string of column names to order by.
   * @param \Zend\Db\Sql\Predicate\Predicate|null $where
   *   Conditions to filter results.
   * @param int|null $depthLimit
   *   Depth in format of number of generations to include.
   * @param bool|null $includeSearchingNode
   *   TRUE if including ancestor node, FALSE if not.
   *
   * @return array
   *   Array of descendant nodes.
   */
  public function findDescendants(
    int $id,
    ?array $columns = NULL,
    $order = NULL,
    ?Predicate $where = NULL,
    ?int $depthLimit = NULL,
    $includeSearchingNode = NULL
  ): array;

  /**
   * Find the children nodes of a given node.
   *
   * @param int $id
   *   The node ID of the parent.
   * @param array|null $columns
   *   Columns from the database to return for each node.
   * @param string|null $order
   *   Comma-separated string of column names to order by.
   * @param \Zend\Db\Sql\Predicate\Predicate|null $where
   *   Conditions to filter results.
   * @param bool|null $includeSearchingNode
   *   TRUE if including parent node, FALSE if not.
   *
   * @return array
   *   Array of child node rows.
   */
  public function findChildren(
    $id,
    $columns = NULL,
    $order = NULL,
    Predicate $where = NULL,
    $includeSearchingNode = NULL
  ): array;

  /**
   * Load list of ancestors of a given node.
   *
   * @param int $id
   *   The node ID of the ancestor.
   * @param array|null $columns
   *   Columns from the database to return for each node.
   * @param string|null $order
   *   Comma-separated string of column names to order by.
   * @param \Zend\Db\Sql\Predicate\Predicate|null $where
   *   Conditions to filter results.
   * @param int|null $depthLimit
   *   Depth in format of number of generations to include.
   * @param bool|null $includeSearchingNode
   *   TRUE if including original node, FALSE if not.
   *
   * @return array
   *   Array of ancestor nodes.
   */
  public function findAncestors(
    $id,
    $columns = NULL,
    $order = NULL,
    Predicate $where = NULL,
    $depthLimit = NULL,
    $includeSearchingNode = NULL
  ): array;

  /**
   * Load the parent of a given node.
   *
   * @param int $id
   *   The node ID of the ancestor.
   * @param array|null $columns
   *   Columns from the database to return for each node.
   * @param bool|null $includeSearchingNode
   *   TRUE if including original node, FALSE if not.
   *
   * @return array|bool|null
   *   The row of the parent node if one exists.
   */
  public function findParent($id, $columns = NULL, $includeSearchingNode = NULL);

  /**
   * Find node IDs matching custom parameters.
   *
   * @param array|null $columns_with_data
   *   Keys are column names (identifiers), values are column values.
   *
   * @return array
   *   Array of nested set ids.
   */
  public function findNestedSetIdsByCustomData(?array $columns_with_data): array;

  /**
   * Find a single node ID associated with a specific EntitreeLocation ID.
   *
   * @param int $locationid
   *   Entitree Location ID.
   *
   * @return int|null
   *   Nested set node ID.
   */
  public function findNestedSetIdByLocationId(int $locationid): ?int;

  /**
   * Wrapper for HybridManipulate::moveMakeChild.
   *
   * @param int $source
   *   Nested set node ID to move.
   * @param int $destination
   *   Nested set node ID of destination parent.
   */
  public function move(int $source, int $destination);

  /**
   * Wrapper for HybridManipulate::delete.
   *
   * Deletes a node and all of its descendants.
   *
   * @param int $node
   *   Nested set node ID to remove.
   */
  public function delete(int $node);

}
