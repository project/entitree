<?php

namespace Drupal\entitree\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\entitree\EntitreeLocationInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Cache\Cache;

/**
 * Defines the EntitreeLocation entity.
 *
 * @ContentEntityType(
 *   id = "entitree_location",
 *   label = @Translation("Entitree Location"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\entitree\Form\EntitreeLocationForm",
 *       "edit" = "Drupal\entitree\Form\EntitreeLocationForm"
 *     },
 *   "list_builder" = "Drupal\Core\Entity\EntityListBuilder"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   base_table = "entitree_location",
 *   admin_permission = "administer entitree"
 * )
 */
class EntitreeLocation extends ContentEntityBase implements EntitreeLocationInterface {

  /**
   * Path segment for this location at the time it was loaded by the store.
   *
   * @var string
   */
  protected $originalPathSegment;

  /**
   * The parent location when this location was loaded.
   *
   * @var \Drupal\entitree\EntitreeLocationInterface
   */
  protected $originalParentLocation = NULL;

  /**
   * The parent location.
   *
   * @var \Drupal\entitree\EntitreeLocationInterface
   */
  protected $parentLocation = NULL;

  /**
   * Identifies if this location is the site root.
   *
   * @var bool
   */
  protected $siteRoot = NULL;

  /**
   * Identifies if this location is the Entitree root.
   *
   * @var bool
   */
  protected $entitreeRoot = FALSE;

  /**
   * {@inheritDoc}
   */
  public static function baseFieldDefinitions(
    EntityTypeInterface $entity_type
  ) {
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the entitree location entity.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    // Unique beyond the scope of Entitree.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the entitree location entity.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setDescription(t('The label of the entitree location entity.'))
      ->setRequired(TRUE)
      ->setReadOnly(FALSE);

    $fields['main_location'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Main Location'))
      ->setDescription(t('Flag to denote the main location for an entity'))
      ->setRequired(TRUE)
      ->setDefaultValue(FALSE);

    $fields['path_segment'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Path segment'))
      ->setDescription(
        t('The path segment of the entitree location entity.')
      )
      ->setReadOnly(FALSE);

    $fields['pid'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('PID'))
      ->setDescription(
        t('The PID path_alias reference of the entitree location entity.')
      )
      ->setReadOnly(FALSE);

    $fields['ref_entity_id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Referenced entity id'))
      ->setDescription(
        t('The referenced entity id of the entitree location entity.')
      )
      ->setRequired(TRUE)
      ->setReadOnly(FALSE);

    $fields['ref_entity_type'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Referenced entity type'))
      ->setDescription(
        t('The referenced entity type of the entitree location entity.')
      )
      ->setRequired(TRUE)
      ->setReadOnly(FALSE);

    $fields['langcode'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Language'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    return $fields;
  }

  /**
   * {@inheritDoc}
   */
  public function getId(): int {
    return (int) $this->get('id')->getString();
  }

  /**
   * {@inheritDoc}
   */
  public function getLabel(): string {
    return $this->get('label')->getString();
  }

  /**
   * {@inheritDoc}
   */
  public function getOriginalParent(): ?EntitreeLocationInterface {
    return $this->originalParentLocation;
  }

  /**
   * {@inheritDoc}
   */
  public function getParent(): ?EntitreeLocationInterface {
    return $this->parentLocation;
  }

  /**
   * {@inheritDoc}
   */
  public function setOriginalparent(?EntitreeLocationInterface $parent) {
    $this->originalParentLocation = $parent;
  }

  /**
   * {@inheritDoc}
   */
  public function setParent(?EntitreeLocationInterface $parent) {
    $this->parentLocation = $parent;
  }

  /**
   * {@inheritDoc}
   */
  public function parentHasChanged(): bool {
    $originalParent = $this->getOriginalParent();
    $parent = $this->getParent();
    return !$originalParent || $parent->getId() !== $originalParent->getId();
  }

  /**
   * {@inheritDoc}
   */
  public function getPathSegment(): string {
    return $this->get('path_segment')->getString();
  }

  /**
   * {@inheritDoc}
   */
  public function getOriginalPathSegment(): string {
    return $this->originalPathSegment;
  }

  /**
   * {@inheritDoc}
   */
  public function pathSegmentHasChanged(): bool {
    return $this->getPathSegment() !== $this->getOriginalPathSegment();
  }

  /**
   * {@inheritDoc}
   */
  public function getLangcode(): string {
    return $this->get('langcode')->getString();
  }

  /**
   * {@inheritDoc}
   */
  public function getReferencedEntityType(): string {
    return $this->get('ref_entity_type')->getString();
  }

  /**
   * {@inheritDoc}
   */
  public function getReferencedEntityId(): int {
    return (int) $this->get('ref_entity_id')->getString();
  }

  /**
   * {@inheritDoc}
   */
  public function getReferencedEntity(): EntityInterface {
    return \Drupal::entityTypeManager()
      ->getStorage(
        $this->getReferencedEntityType()
      )->load(
        $this->getReferencedEntityId()
      );
  }

  /**
   * {@inheritDoc}
   */
  public function isSiteRoot(): bool {
    return $this->siteRoot !== NULL ? $this->siteRoot : $this->isHome();
  }

  /**
   * {@inheritDoc}
   */
  public function setSiteRoot(bool $value): EntitreeLocationInterface {
    $this->siteRoot = $value;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function isEntitreeRoot(): bool {
    return $this->entitreeRoot;
  }

  /**
   * {@inheritDoc}
   */
  public function setEntitreeRoot(bool $value): EntitreeLocationInterface {
    $this->entitreeRoot = $value;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function isHome(): bool {
    return \Drupal::state()->get('entitree.site_root_location') == $this->getId();
  }

  /**
   * {@inheritDoc}
   */
  public function isMainLocation(): bool {
    return (int) $this->get('main_location')->getString() === 1;
  }

  /**
   * Determine if this location has already been added to the tree.
   *
   * @return bool
   *   TRUE if added to the tree. FALSE if not added to the tree.
   */
  protected function hasBeenAddedToTree(): bool {
    // For now assume any location without an original parent hasn't been added.
    return !!$this->getOriginalParent() || $this->isSiteRoot();
  }

  /**
   * {@inheritDoc}
   */
  public function getChildren(): array {
    if (!$this->hasBeenAddedToTree()) {
      return [];
    }

    // Load and return the children from the store.
    return \Drupal::service('entitree.manager')
      ->getStore($this->getLangcode())
      ->getLocationChildren($this);
  }

  /**
   * {@inheritDoc}
   */
  public function getAncestors(): array {
    return \Drupal::service('entitree.manager')
      ->getStore($this->getLangcode())
      ->getLocationAncestors($this->getId());
  }

  /**
   * {@inheritDoc}
   */
  public function getPath(): string {
    if ($this->isSiteRoot()) {
      return '/';
    }

    $result = \Drupal::service("database")
      ->query("SELECT alias from {path_alias} where id = :pid", [
        ":pid" => $this->get('pid')->getString(),
      ])->fetchObject();

    // The site root returns only a slash.
    if (isset($result->alias)) {
      return $result->alias;
    }
    return '/';
  }

  /**
   * Generate and return the full path string to this location.
   *
   * This by default does not use pending path segments and should match the
   * path currently saved in the database.
   *
   * @return string
   *   The full path of this location.
   */
  protected function generatePath(): string {
    // The site root is always /.
    if ($this->isSiteRoot()) {
      return '/';
    }

    // Get the parent's path.
    $parentPath = '';
    if ($this->getParent()) {
      $parentPath = $this->getParent()->getPath();
    }

    $path = $parentPath === '/'
      ? $parentPath . $this->getPathSegment()
      : $parentPath . "/" . $this->getPathSegment();

    return $path;
  }

  /**
   * {@inheritDoc}
   */
  public function updatePathAlias() {
    // Only generate a path alias if this location has a path segment.
    if ($this->getPathSegment()) {
      $path = $this->generatePath();

      // Update path alias.
      $entitreeEntityTypeManager = \Drupal::service('plugin.manager.entitree_entity_type');
      $plugin = $entitreeEntityTypeManager
        ->getEntitreeEntityType($this->getReferencedEntityType());

      /*
       * TODO figure out approach for proper language code handling.
       */
      $pid = $this->get('pid')->getString();
      if ($pid) {
        $pathAlias = \Drupal::entityTypeManager()->getStorage('path_alias')->load($pid);
        $pathAlias->alias = $path;
        $pathAlias->save();
      }
      else {
        $pathAlias = \Drupal::entityTypeManager()->getStorage('path_alias')
          ->create([
            'path' => $plugin->getSourcePath($this->getReferencedEntityId()),
            'alias' => $path,
            'langcode' => $this->getLangcode(),
          ]);
        $pathAlias->save();
        $pid = $pathAlias->id();
      }

      // Clear breadcrumb cache tag to make breadrumb update in admin interface.
      Cache::invalidateTags(['entitree.breadcrumb']);
      $this->pid = $pid;

      /*
       * TODO this for some sites will need to update thousands of locations.
       * Need to find a safer way to handle this
       */
      foreach ($this->getChildren() as $child) {
        $child->updatePathAlias();
        $child->save();
      }

      /*
       * TODO Invalidate cache for local actions/tasks (whichever is tabs).
       */
    }
  }

  /**
   * {@inheritDoc}
   */
  public function deletePathAlias(): EntitreeLocationInterface {
    if ($pid = $this->get('pid')->getString()) {
      $pathAlias = \Drupal::entityTypeManager()->getStorage('path_alias')
        ->load($pid);
      $pathAlias->delete();
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function applyEntitreeUpdates(
    bool $update,
    EntitreeLocationInterface $newParent = NULL
  ): EntitreeLocationInterface {
    if (!$update) {
      // This location is new. Add it to the store.
      $store = \Drupal::service('entitree.manager')
        ->getStore($this->getLangcode());
      $store->insertLocation($this, $newParent);
    }
    /*
     * TODO If this is not a new location but its location has changed update
     * with the nested set manager.
     */
    else {
      // This location already existed.
      /*
       * If a new parent was provided and it is different from the parent in the
       * store, move the location.
       */
      $originalParent = $this->getOriginalParent();
      if ($newParent && $originalParent && $newParent->getId() != $originalParent->getId()) {
        \Drupal::service('entitree.manager')
          ->getStore(
            $this->getLangcode()
          )->moveLocation(
            $this->getId(),
            $newParent->getId()
          );
      }
    }

    /*
     * After installation of the entitree module is complete a state value will
     * be set. Don't try to load the store until then.
     */
    if (\Drupal::state()->get('entitree.initial_tree_created')) {
      /*
       * TODO invalidate the store matching the language of this location
       */
    }

    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    /*
     * If this is a new location and this entity doesn't yet have any locations
     * mark this as the main location.
     */
    if ($this->isNew()) {
      // Load locations for this entity.
      $locations = \Drupal::service('entitree.manager')
        ->getLocationsByIdAndType(
          $this->getReferencedEntityId(),
          $this->getReferencedEntityType()
        );

      if (count($locations) === 0) {
        $this->set('main_location', TRUE);
      }
    }

    // Stop here if this is the entitree root.
    if ($this->isEntitreeRoot()) {
      return;
    }

    // If the referenced entity is an empty location update its label.
    if ($this->getReferencedEntityType() === 'empty_location') {
      /** @var \Drupal\entitree\Entity\EmptyLocation $referencedEntity */
      $referencedEntity = $this->getReferencedEntity();
      $referencedEntity->set('label', $this->getLabel());
      $referencedEntity->save();

      /*
       * Empty locations don't have a canonical route, but the source path is
       * always this pattern.
       */
      $internalPath = $this->isNew ? NULL : '/location/' . $referencedEntity->id();
    }
    else {
      // Get the internal (source) path for the path_alias table.
      $internalPath = $this->isNew ? NULL : '/' . $this->getReferencedEntity()->toUrl()->getInternalPath();
    }

    // Ensure the path is unique if this isn't the site root.
    if (!$this->isSiteRoot()) {
      /*
       * If the path alias is already in use by another source make the path
       * segment unique.
       */
      $uniquePathSegment = \Drupal::service('entitree.alias_uniquifier')->uniquify(
        $this->generatePath(),
        $internalPath,
        $this->getLangcode()
      );

      $this->set('path_segment', $uniquePathSegment);

      // If the path segment or parent changed updateh the path alias.
      if ($this->parentHasChanged() || $this->pathSegmentHasChanged()) {
        $this->updatePathAlias();
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function postLoad(
    EntityStorageInterface $storage,
    array &$entities
  ) {
    parent::postLoad($storage, $entities);

    foreach ($entities as $entity) {
      /** @var \Drupal\entitree\EntitreeLocationInterface $entity */
      // Populate the parentLocation property.
      if (!$entity->isSiteRoot()) {
        $parent = \Drupal::service('entitree.manager')
          ->getStore($entity->getLangcode())
          ->getLocationParent($entity->getId());

        $entity->setOriginalParent($parent);
        $entity->setParent($parent);
      }

      /*
       * Set the path segment to the original path segment property to track
       * changes.
       */
      $entity->originalPathSegment = $entity->get('path_segment')->getString();
    }
  }

  /**
   * {@inheritDoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    foreach ($entities as $location) {
      /** @var \Drupal\entitree\EntitreeLocationInterface $location */
      $location->deletePathAlias();

      // If referenced entity is an empty location delete it.
      if ($location->getReferencedEntityType() === 'empty_location') {
        $location->getReferencedEntity()->delete();
      }
    }

    /*
     * TODO invalidate store cache.
     */
  }

  /**
   * {@inheritDoc}
   */
  public function deleteDescendants() {
    $store = \Drupal::service('entitree.manager')->getStore($this->getLangcode());

    // Load all descendants.
    $descendants = $store->getLocationDescendants($this->getId());

    // Delete all descendant locations.
    \Drupal::entityTypeManager()->getStorage('entitree_location')->delete($descendants);

    // Remove this range from the store.
    $store->removeLocationAndDescendants($this->getId());
  }

}
