<?php

namespace Drupal\entitree\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the EmptyLocation entity.
 *
 * @ContentEntityType(
 *   id = "empty_location",
 *   label = @Translation("Empty Location"),
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid"
 *   },
 *   base_table = "empty_location",
 *   admin_permission = "administer entitree",
 *   handlers = {
 *    "list_builder" = "Drupal\Core\Entity\EntityListBuilder"
 *   }
 * )
 */
class EmptyLocation extends ContentEntityBase {

  /**
   * {@inheritDoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the location entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the location entity.'))
      ->setReadOnly(TRUE);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setDescription(t('The label of the location entity.'))
      ->setReadOnly(FALSE);

    return $fields;
  }

}
