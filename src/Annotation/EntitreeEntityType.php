<?php

namespace Drupal\entitree\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Entitree entity type item annotation object.
 *
 * @see \Drupal\entitree\Plugin\EntitreeEntityTypeManager
 * @see plugin_api
 *
 * @Annotation
 */
class EntitreeEntityType extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
