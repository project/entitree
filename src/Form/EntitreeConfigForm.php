<?php

namespace Drupal\entitree\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entitree\EntitreeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Handles building, validating, and submitting the Entitree config form.
 */
class EntitreeConfigForm extends FormBase {

  /**
   * The entitree manager service.
   *
   * @var \Drupal\entitree\EntitreeManagerInterface
   */
  protected $entitreeManager;

  /**
   * {@inheritDoc}
   */
  public function __construct(EntitreeManagerInterface $entitreeManager) {
    $this->entitreeManager = $entitreeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('entitree.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entitree_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['entitree_enabled_entity_types'] = $this->buildEntityTypesField();
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Store selected types.
    $selectedTypes = $this->getSelectedCheckboxes($form_state->getValue("entitree_enabled_entity_types"));
    $this->entitreeManager->updateConfig("types", $selectedTypes);

    /*
     * Generate list of available operations for each entity type as store in
     * config.
     */
    $this->entitreeManager->generateEntityTypeOperations($selectedTypes);

    $messenger = \Drupal::messenger();
    $messenger->addMessage('Your settings have been saved', $messenger::TYPE_STATUS);

    /*
     * TODO figure out how to invalidate the cache to make the operations
     * properly render
     * https://drupal.stackexchange.com/questions/213235/clearing-local-tasks-cache
     */
    // \Drupal::cache('menu')->invalidateAll();
  }

  /**
   * Generate an array of checkbox keys based on which are checked.
   *
   * @param array $checkboxesValue
   *   Array of values of a checkboxes field.
   *
   * @return array
   *   Array of keys of selected checkboxes.
   */
  private function getSelectedCheckboxes(array $checkboxesValue): array {
    $selected = [];

    foreach ($checkboxesValue as $key => $value) {
      if ($value) {
        $selected[] = $key;
      }
    }

    // Always make empty_location selected.
    $selected[] = 'empty_location';

    return $selected;
  }

  /**
   * Build the Form API structure to list each entity type as a checkbox.
   *
   * @return array
   *   Form API array for the new field.
   */
  private function buildEntityTypesField(): array {
    // Get enabled types from the entitree entity type manager.
    $types = \Drupal::service('plugin.manager.entitree_entity_type');

    $options = [];

    // Load previously selected entity types.
    $selectedTypes = $this->entitreeManager->getConfig()->get('types');

    // TODO auto-select and hide empty location value.
    foreach ($types->getDefinitions() as $key => $definition) {
      if ($definition['entity_id'] !== 'empty_location') {
        $options[$definition['entity_id']] = $definition['entity_name'];
      }
    }

    $field = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled Entity Types'),
      '#description' => $this->t('Select the entity types that can be added to the Entitree structure'),
      '#options' => $options,
      '#weight' => '0',
      '#default_value' => $selectedTypes,
    ];

    return $field;
  }

}
