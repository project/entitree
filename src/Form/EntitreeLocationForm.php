<?php

namespace Drupal\entitree\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\entitree\EntitreeLocationInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\entitree\EntitreeManagerInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;

/**
 * Form for creating and editing Entitree locations.
 */
class EntitreeLocationForm extends ContentEntityForm {

  /**
   * Drupal\entitree\EntitreeManagerInterface definition.
   *
   * @var \Drupal\entitree\EntitreeManagerInterface
   */
  protected $entitreeManager;

  /**
   * Constructs a new EntitreeLocationForm object.
   */
  public function __construct(
    EntitreeManagerInterface $entitree_manager,
    EntityRepositoryInterface $entity_repository,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    $time
  ) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->entitreeManager = $entitree_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entitree.manager'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->has('datetime.time') ? $container->get('datetime.time') : NULL
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entitree_location_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    EntitreeLocationInterface $location = NULL,
    EntitreeLocationInterface $parent = NULL
  ) {
    $form = parent::buildForm($form, $form_state);
    $storage = $form_state->getStorage();

    if ($location) {
      $this->entity = $location;
    }

    /** @var \Drupal\entitree\EntitreeLocationInterface $location */
    $location = $this->entity;

    $parentPath = "/";
    // For new locations, the parent is provided in the form state storage.
    if (isset($storage['parent'])) {
      $parent = $storage['parent'];
    }
    elseif (!$location->isNew()) {
      /*
       * When updating locations, if the parent was provided as an argument
       * then use that. Otherwise fall back to the existing parent.
       */
      if ($parent) {
        $storage['parent'] = $parent;
      }
      else {
        $parent = $location->getParent();
      }
    }

    /*
     * @todo If the selected parent is the homepage or a descendant of the
     * homepage, include the path as a prefix for the path segment field.
     */
    if ($parent) {
      $parentPath = $parent->getPath();
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#size' => 64,
      '#weight' => '0',
      '#default_value' => $location->getLabel(),
      '#required' => TRUE,
    ];

    /*
     * @todo only show path segment field when this is a descendant of the
     * homepage.
     */
    $form['path_segment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path Segment'),
      '#maxlength' => 255,
      '#size' => 64,
      '#weight' => '1',
      '#default_value' => $location->getPathSegment(),
      '#required' => TRUE,
      '#field_prefix' => $parentPath === "/" ? $parentPath : $parentPath . "/",
    ];

    /*
     * If editing an existing location, add a link to change the parent
     * This is also only available for non-root locations as you can't move the
     * root
     */
    if (!$location->isNew() && $parent) {
      $form['change_location'] = [
        '#prefix' => "<div>",
        '#markup' => Link::fromTextAndUrl($this->t("Change Location"),
          Url::fromRoute(
            'entitree.entitree_display_controller_renderTreeAndEdit',
            [
              'parent' => $parent ? $parent->getId() : NULL,
              'location' => $location->getId(),
            ])
        )->toString(),
        "#suffix" => "</div>",
        '#weight' => 2,
      ];
    }

    // Update storage in case parent changed.
    $form_state->setStorage($storage);
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\entitree\EntitreeLocationInterface $location */
    $location = $this->entity;

    $storage = $form_state->getStorage();

    // Set the location parent.
    if (isset($storage['parent'])) {
      $location->setParent($storage['parent']);
    }

    $isNew = $location->isNew();
    $status = $location->save();
    $location->applyEntitreeUpdates(!$isNew, isset($storage['parent'])
      ? $storage['parent'] : NULL);

    if ($status) {
      \Drupal::messenger()->addMessage($this->t('The location has been saved'));
    }
    else {
      \Drupal::messenger()->addError(
        $this->t('There was an error while saving this location'));
    }

    /*
     * Redirect back to path defined by entity type plugin unless it's an empty
     * location.
     */
    if ($location->getReferencedEntityType() === 'empty_location') {
      $form_state->setRedirectUrl(Url::fromRoute('entitree.entitree_display_controller_RenderTreeDisplay', [
        'parent' => $location->getParent()->getId(),
      ]));
    }
    else {
      $form_state->setRedirectUrl(
        \Drupal::service('plugin.manager.entitree_entity_type')
          ->getEntitreeEntityType($location->getReferencedEntityType())
          ->getLocationsPathById($location->getReferencedEntityId()));
    }
  }

}
