<?php

namespace Drupal\entitree\Form;

use Drupal\entitree\EntitreeLocationInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a confirmation form to confirm deletion of something by id.
 *
 * TODO this form is a holdover from before locations were entities. This
 * should be converted to use the entity form system like add/edit have been.
 */
class DeleteLocationForm extends FormBase {

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntitreeLocationInterface $location = NULL) {
    // Show different options if this location has children.
    if (count($location->getChildren()) > 0) {
      $form['help'] = [
        '#markup' => "<p>" . t("If you are sure you want to delete this location, select the appropriate method. You can replace this location with an empty location to maintain the current structure, or you can delete this location and all of its descendants. This cannot be undone.") . "</p>"
      ];

      $form['delete_replace_empty'] = [
        "#type" => "submit",
        "#submit" => ["::replaceWithEmpty"],
        '#name' => 'delete_replace_empty',
        "#value" => t("Delete and replace with an empty location"),
      ];

      $form['delete_with_descendants'] = [
        "#type" => "submit",
        "#submit" => ["::deleteWithDescendants"],
        '#name' => 'delete_with_descendants',
        "#value" => t("Delete location and all descendants"),
      ];
    }
    else {
      $form['help'] = [
        '#markup' => "<p>" . t('If you are sure you want to delete this location, click the "Delete this location" button below to continue. This location has no children, and can be deleted immediately. This cannot be undone.') . "</p>"
      ];

      $form['delete_single_location'] = [
        "#type" => "submit",
        "#submit" => ["::deleteSingleLocation"],
        '#name' => 'delete_single_location',
        "#value" => t("Delete this location"),
      ];
    }

    $storage = $form_state->getStorage();
    $storage['location'] = $location;
    $form_state->setStorage($storage);

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function getTitle(EntitreeLocationInterface $location) {
    return t("Are you sure you want to delete location %label?", ["%label" => $location->getLabel()]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // All submit buttons use custom methods, so this should never be called.
    throw new \Exception("An invalid location delete option was selected");
  }

  /**
   * Convert this location to be an empty location.
   *
   * @param array $form
   *   The Form API array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function replaceWithEmpty(array &$form, FormStateInterface $form_state) {
    \Drupal::messenger()->addWarning('This is not yet supported');
    // TODO complete logic once empty location support is added.
  }

  /**
   * Delete the location and all of its descendants.
   *
   * @param array $form
   *   The Form API array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function deleteWithDescendants(array &$form, FormStateInterface $form_state) {
    $storage = $form_state->getStorage();

    $location = $storage["location"];

    if (!$location) {
      throw new \Exception(t("Cannot delete the location because none were associated with this form"));
    }

    // Get the full path of this location to use in the confirmation message.
    $fullPath = $location->getPath();

    // Set the redirect.
    $entitreeType = \Drupal::service('plugin.manager.entitree_entity_type')->getEntitreeEntityType($location->getReferencedEntityType());
    $form_state->setRedirectUrl($entitreeType->getLocationsPathById($location->getReferencedEntityId()));

    $location->deleteDescendants();

    // Delete entity.
    $location->delete();

    \Drupal::messenger()->addMessage(t("The location at %path, and all of its descendants, have been deleted", ["%path" => $fullPath]));
  }

  /**
   * Delete a location without any descendants.
   *
   * @param array $form
   *   The Form API array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   */
  public function deleteSingleLocation(array &$form, FormStateInterface $form_state) {
    $storage = $form_state->getStorage();

    $location = $storage["location"];

    if (!$location) {
      throw new \Exception(t("Cannot delete the location because none were associated with this form"));
    }

    // Get the full path of this location to use in the confirmation message.
    $fullPath = $location->getPath();

    // Set the redirect.
    $entitreeType = \Drupal::service('plugin.manager.entitree_entity_type')->getEntitreeEntityType($location->getReferencedEntityType());
    $form_state->setRedirectUrl($entitreeType->getLocationsPathById($location->getReferencedEntityId()));

    // Delete from store.
    $location->deleteDescendants();

    // Delete entity.
    $location->delete();

    \Drupal::messenger()->addMessage(t("The location at %path has been deleted", ["%path" => $fullPath]));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() : string {
    return "location_delete_form";
  }

}
