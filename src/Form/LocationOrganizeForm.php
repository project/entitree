<?php

namespace Drupal\entitree\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a confirmation form to confirm organization of an entity's locations.
 */
class LocationOrganizeForm extends ConfirmFormBase {

  /**
   * ID of the entity to organize.
   *
   * @var int
   */
  protected $id;

  /**
   * Type of the entity to organize.
   *
   * @var string
   */
  protected $type;

  /**
   * Plugin for this entitree location type.
   *
   * @var EntitreeEntityTypeInterface
   */
  protected $plugin;

  /**
   * Entity to organize.
   *
   * @var EntityInterface
   */
  protected $entity;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL, string $entity_type = NULL) {
    $this->id = (int) $id;
    $this->type = $entity_type;
    $entitreeEntityTypeManager = \Drupal::service('plugin.manager.entitree_entity_type');
    $this->plugin = $entitreeEntityTypeManager->getEntitreeEntityType($this->type);
    $this->entity = $this->plugin->loadEntityById($id);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo: Remove duplicates and create locations for all other orphaned path aliases.
    // Load all orphaned path aliases.
    /** @var \Drupal\path_alias\PathAliasInterface[] $orphanedPathAliases */
    $orphanedPathAliases = $this->plugin::getOrphanedpathAliases($this->entity->id());

    // Load all locations for this entity.
    /** @var \Drupal\entitree\EntitreeManagerInterface $entitreeManager */
    $entitreeManager = \Drupal::service('entitree.manager');
    $locations = $entitreeManager->getLocationsByIdAndType($this->id, $this->type);

    $deleted = $created = 0;

    // For each orphaned alias, check to see if it's a duplicate.
    foreach ($orphanedPathAliases as $alias) {
      foreach ($locations as $location) {
        if ($alias->get('alias')->getString() == $location->getPath()) {
          /*
           * This is a duplicate because the path alias' path and alias match
           * the location's.
           */
          $alias->delete();
          $deleted++;
        }
        else {
          // Create necessary locations to make this path work.
        }
      }
    }

    $message = ':deleted path alias' . ($deleted == 1 ? ' was' : 'es were')
      . ' deleted and :created' . ($created == 1 ? ' was' : ' were')
      . ' used to create new locations for this entity';

    \Drupal::messenger()->addMessage($this->t(
      $message,
      [
        ':deleted' => $deleted,
        ':created' => $created,
      ]));

    $form_state->setRedirectUrl($this->plugin->getLocationsPathById($this->id));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return "entitree_location_organize_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return $this->plugin->getLocationsPathById($this->id);
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return t('Are you sure you want to organize locations for %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritDoc}
   */
  public function getDescription() {
    return t('This will delete any duplicate path aliases and create locations for all others.');
  }

}
