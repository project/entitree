<?php

namespace Drupal\entitree;

use Drupal\core\Entity\EntityInterface;

/**
 * Interface for the Entitree manager service.
 */
interface EntitreeManagerInterface {

  /**
   * For the given langcode generate a nested set factory if needed and return.
   *
   * @param string $langcode
   *   Two character Drupal langcode.
   *
   * @return NestedSetFactory
   *   Nested set factory for the specified langcode.
   */
  public function getNestedSetFactory(string $langcode): NestedSetFactory;

  /**
   * Get the configuration object for Entitree.
   */
  public function getConfig();

  /**
   * Update a configuration value in the Entitree config object.
   *
   * @param string $name
   *   Name of the configuration value to update.
   * @param mixed $value
   *   Value to set in the config object.
   */
  public function updateConfig(string $name, $value);

  /**
   * Get an Entitree store associated with a specific langcode.
   *
   * @param string $langcode
   *   Two letter langcode associated with the store.
   *
   * @return \Drupal\entitree\EntitreeStoreInterface
   *   The Entitree store associated with the provided langcode.
   */
  public function getStore(string $langcode): EntitreeStoreInterface;

  /**
   * Get all of an entity's locations by its entity ID and type.
   *
   * @param int $entityId
   *   Numeric ID of the entity.
   * @param string $entityType
   *   Entity type string.
   *
   * @return array|null
   *   Array of Entitree location entities if any exist.
   */
  public function getLocationsByIdAndType(int $entityId, string $entityType): ?array;

  /**
   * Given a full path get the associated EntitreeLocation entity.
   *
   * @param string $path
   *   The full path starting with "/".
   *
   * @return EntitreeLocationInterface|null
   *   The EntitreeLocation entity if one is associated with the path.
   */
  public function getLocationByPath(string $path): ?EntitreeLocationInterface;

  /**
   * Apply token replacements using the context of the provided entity.
   *
   * @param string $value
   *   The string to receive the replacements.
   * @param \Drupal\Core\EntityInterface $entity
   *   The entity to use as context.
   *
   * @return string
   *   The string with replacements.
   */
  public function applyEntityTokenReplaceAction(string $value, EntityInterface $entity): string;

  /**
   * Load the Entitree site root location entity.
   *
   * @param string $langcode
   *   Two letter langcode matching the entitree store to use.
   *
   * @return \Drupal\entitree\EntitreeLocationInterface
   *   Entitree location entity representing the site root.
   */
  public function getSiteRootLocation(string $langcode): EntitreeLocationInterface;

  /**
   * Load and return the main location for this entity if it exists.
   *
   * @param \Drupal\Core\EntityInterface $entity
   *   Entity referened by the location to find.
   *
   * @return \Drupal\entitree\EntitreeLocationInterface|null
   *   Main Entitree location if one exists.
   */
  public function getMainLocation(EntityInterface $entity): ?EntitreeLocationInterface;

  /**
   * Get the two letter langcode associated with a specific location.
   *
   * @param int $locationId
   *   ID of the location to fetch.
   *
   * @return string
   *   Two letter langcode of the location.
   */
  public function getLangcodeByLocation(int $locationId): string;

  /**
   * Generate and store entity operations in config.
   *
   * @todo this is called when the entitree config form is saved, but it should
   * probably be called whenever a module is enabled. It's only necessary for
   * modules with an EntitreeEntityType plugin or EntitreeOperationsEvent
   * subscriber
   *
   * @param string[] $types
   *   Array of type strings.
   */
  public function generateEntityTypeOperations(array $types): array;

  /**
   * Load config and reformat into associative array.
   *
   * @return array
   *   Array of operations available for the entity type.
   */
  public function getEntityTypeOperations(): array;

  /**
   * Generate and return an array of token types used by each entity type.
   *
   * @return array
   *   Array of token types.
   */
  public function getAllSupportedTokenTypes(): array;

  /**
   * Generate a Form API array showing the Entitree structure with actions.
   *
   * @param string $wrapper
   *   Selector of the wrapper element.
   * @param Drupal\entitree\EntitreeLocationInterface $parent
   *   The currently selected parent location.
   * @param string $pageHelp
   *   The help text to display with the form elements.
   *
   * @return array
   *   Form API array containing all prepared form elements.
   */
  public function generateLocationSelectionFormArray(string $wrapper, EntitreeLocationInterface $parent = NULL, string $pageHelp = ""): array;

}
