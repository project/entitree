<?php

namespace Drupal\entitree;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides an interface defining an EntitreeLocation entity.
 */
interface EntitreeLocationInterface extends ContentEntityInterface {

  /**
   * Get the entity ID of this location.
   *
   * @return int
   *   Entity ID of the location entity.
   */
  public function getId(): int;

  /**
   * Load EntitreeLocation children entities.
   *
   * @return array
   *   Array of EntitreeLocation entities.
   */
  public function getChildren(): array;

  /**
   * Get the full path to a location.
   *
   * @return string
   *   Full path for the location.
   */
  public function getPath(): string;

  /**
   * Store the generated path using the alias storage service.
   */
  public function updatePathAlias();

  /**
   * Delete the related pid.
   *
   * If this location has a stored pid relationship, delete the alias using the
   * path service.
   *
   * @return \Drupal\entitree\Entity\EntitreeLocationInterface
   *   The EntitreeLocation entity.
   */
  public function deletePathAlias(): EntitreeLocationInterface;

  /**
   * Get the label string for this location.
   *
   * @return string
   *   Label for this location.
   */
  public function getLabel(): string;

  /**
   * Get the parent location.
   *
   * @return EntitreeLocationInterface|null
   *   The parent location if not the site root.
   */
  public function getParent(): ?EntitreeLocationInterface;

  /**
   * Set the parent location at the time the location was loaded.
   *
   * @param \Drupal\entitree\EntitreeLocationInterface|null $parent
   *   The parent location.
   */
  public function setOriginalparent(?EntitreeLocationInterface $parent);

  /**
   * Set the parent location.
   *
   * @param EntitreeLocationInterface|null $parent
   *   The parent location.
   */
  public function setParent(?EntitreeLocationInterface $parent);

  /**
   * Get the path segment for this location.
   *
   * @return string
   *   Path segment for this location.
   */
  public function getPathSegment(): string;

  /**
   * Get the path segment of the location from when it was loaded.
   *
   * @return string
   *   The last-saved path segment for this location.
   */
  public function getOriginalPathSegment(): string;

  /**
   * Determine if the path segment for this location has changed.
   *
   * @return bool
   *   TRUE/FALSE flag to identify if the path segment has changed.
   */
  public function pathSegmentHasChanged(): bool;

  /**
   * Get the language code for this location.
   *
   * @return string
   *   The language code.
   */
  public function getLangcode(): string;

  /**
   * Get the type of the referenced entity.
   *
   * @return string
   *   The entity type.
   */
  public function getReferencedEntityType(): string;

  /**
   * Get the ID of the referenced entity.
   *
   * @return int
   *   The ID of the referenced entity.
   */
  public function getReferencedEntityId(): int;

  /**
   * Get the referenced entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The referenced entity.
   */
  public function getReferencedEntity(): EntityInterface;

  /**
   * Determine if this location is the site root location.
   *
   * @return bool
   *   Identifier of site root status.
   */
  public function isSiteRoot(): bool;

  /**
   * Set the site root value.
   *
   * @param bool $value
   *   TRUE if site root. FALSE if not site root.
   *
   * @return \Drupal\entitree\EntitreeLocationInterface
   *   Entitree location after update.
   */
  public function setSiteRoot(bool $value): EntitreeLocationInterface;

  /**
   * Applies tree structure updates to this location.
   *
   * @param bool $update
   *   Flag to identify if this location is new or an update.
   * @param EntitreeLocationInterface $newParent
   *   The new parent of this location if applicable.
   *
   * @return EntitreeLocationInterface
   *   The updated location.
   */
  public function applyEntitreeUpdates(
    bool $update,
    EntitreeLocationInterface $newParent = NULL
  ): EntitreeLocationInterface;

  /**
   * Get array of ancestors starting at the parent location.
   *
   * @return array
   *   Array of ancestors' Entitree locations starting with the parent location.
   */
  public function getAncestors(): array;

  /**
   * Get the parent of this location at the time it was loaded from the store.
   *
   * @return \Drupal\entitree\EntitreeLocationInterface|null
   *   The original parent location if one exists.
   */
  public function getOriginalParent(): ?EntitreeLocationInterface;

  /**
   * Determine if the parent has been changed since this location was loaded.
   *
   * @return bool
   *   TRUE if parent has changed. FALSE is parent has not changed.
   */
  public function parentHasChanged(): bool;

  /**
   * Determine if this location is the Entitree root location.
   *
   * @return bool
   *   TRUE if is the Entitree root. FALSE if not the Entitree root.
   */
  public function isEntitreeRoot(): bool;

  /**
   * Set the entitreeRoot value.
   *
   * @param bool $value
   *   Value to assign to the entitreeRoot property.
   *
   * @return \Drupal\entitree\EntitreeLocationInterface
   *   The Entitree location after update.
   */
  public function setEntitreeRoot(bool $value): EntitreeLocationInterface;

  /**
   * Specifies if this is the site home page.
   *
   * @return bool
   *   TRUE if the home page. FALSE if not the home page.
   */
  public function isHome(): bool;

  /**
   * Determine if this is the main location for this entity.
   *
   * @return bool
   *   TRUE if the main location. FALSE if not the main location.
   */
  public function isMainLocation(): bool;

  /**
   * Delete all descendants of this location.
   */
  public function deleteDescendants();

}
