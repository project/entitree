<?php

namespace Drupal\entitree;

use metalinspired\NestedSet\Config;
use metalinspired\NestedSet\HybridFind;
use metalinspired\NestedSet\HybridManipulate;
use Zend\Db\Sql\Predicate\Predicate;

/**
 * Factory class to surface methods that manipulate the store for Entitree.
 */
class NestedSetFactory implements NestedSetFactoryInterface {

  /**
   * Configuration settings containing database connection information.
   *
   * @var \metalinspired\NestedSet\Config
   */
  protected $config;

  /**
   * Instance of the nested set HybridFind class.
   *
   * @var \metalinspired\NestedSet\HybridFind
   */
  protected $find;

  /**
   * Instance of the nested set HybridManipulate class.
   *
   * @var \metalinspired\NestedSet\HybridManipulate
   */
  protected $manipulate;

  /**
   * {@inheritDoc}
   */
  public function __construct(string $langcode) {
    $this->createConfig($langcode);

    $this->find = new HybridFind($this->config);
    $this->manipulate = new HybridManipulate($this->config);
  }

  /**
   * Create the config object to connect to the database.
   *
   * @param string $langcode
   *   Two-letter langcode for the corresponding Entitree structure.
   */
  protected function createConfig(string $langcode) {
    $connectionOptions = \Drupal::database()->getConnectionOptions();
    $port = isset($connectionOptions['port']) ? $connectionOptions['port'] : 3306;
    $dsn = $connectionOptions['driver'] . ':dbname=' . $connectionOptions['database'] . ';host=' . $connectionOptions['host'] . ':' . $port;
    $this->config = Config::createWithDsn($dsn, $connectionOptions['username'], $connectionOptions['password']);
    $this->config->table = 'entitree_structure_' . $langcode;
  }

  /**
   * {@inheritDoc}
   */
  public function getConfig(): Config {
    return $this->config;
  }

  /**
   * {@inheritDoc}
   */
  public function getRootNodeId() : int {
    return (int) $this->find->getRootNodeId();
  }

  /* MANIPULATE METHODS */

  /**
   * {@inheritDoc}
   */
  public function createRootNode() {
    $this->manipulate->createRootNode();
  }

  /**
   * {@inheritDoc}
   */
  public function insert(int $parent, array $data = []) {
    return $this->manipulate->insert($parent, $data);
  }

  /* FIND METHODS */

  /**
   * {@inheritDoc}
   */
  public function findDescendants(
    int $id,
    ?Array $columns = NULL,
    $order = NULL,
    ?Predicate $where = NULL,
    ?int $depthLimit = NULL,
    $includeSearchingNode = NULL
  ): array {
    $result = $this->find->findDescendants($id, $columns, $order, $where, $depthLimit, $includeSearchingNode);

    $descendants = [];
    while ($row = $result->next()) {
      $descendants[] = $row;
    }
    return $descendants;
  }

  /**
   * {@inheritDoc}
   */
  public function findChildren(
    $id,
    $columns = NULL,
    $order = NULL,
    Predicate $where = NULL,
    $includeSearchingNode = NULL
  ): array {
    $result = $this->find->findChildren($id, $columns, $order, $where, $includeSearchingNode);

    $children = [];
    while ($row = $result->next()) {
      $children[] = $row;
    }

    return $children;
  }

  /**
   * {@inheritDoc}
   */
  public function findAncestors(
    $id,
    $columns = NULL,
    $order = NULL,
    Predicate $where = NULL,
    $depthLimit = NULL,
    $includeSearchingNode = NULL
  ): array {
    $result = $this->find->findAncestors($id, $columns, $order, $where, $depthLimit, $includeSearchingNode);

    $ancestors = [];
    while ($row = $result->next()) {
      $ancestors[] = $row;
    }
    return $ancestors;
  }

  /**
   * {@inheritDoc}
   */
  public function findParent($id, $columns = NULL, $includeSearchingNode = NULL) {
    $result = $this->find->findParent($id, $columns, $includeSearchingNode);
    /** @var array $parentRow */
    $parentRow = $result->next();
    return $parentRow;
  }

  /**
   * {@inheritDoc}
   */
  public function findNestedSetIdsByCustomData(?array $columns_with_data): array {
    $result = $this->find->findIdsByCustomData($columns_with_data);

    $ids = [];
    while ($row = $result->next()) {
      $ids[] = $row['id'];
    }
    return $ids;
  }

  /**
   * {@inheritDoc}
   */
  public function findNestedSetIdByLocationId(int $locationid): ?int {
    $ids = $this->findNestedSetIdsByCustomData(['location_id' => $locationid]);
    if (count($ids) == 1) {
      return($ids[0]);
    }
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function move(int $source, int $destination) {
    $this->manipulate->moveMakeChild($source, $destination);
  }

  /**
   * {@inheritDoc}
   */
  public function delete(int $node) {
    $this->manipulate->delete($node);
  }

}
