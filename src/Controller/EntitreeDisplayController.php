<?php

namespace Drupal\entitree\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\entitree\EntitreeLocationInterface;
use Drupal\entitree\Entity\EntitreeLocation;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\entitree\EntitreeManagerInterface;
use Drupal\entitree\Entity\EmptyLocation;

/**
 * Controller for generating display for Entitree routes.
 */
class EntitreeDisplayController extends ControllerBase {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\entitree\EntitreeManagerInterface definition.
   *
   * @var \Drupal\entitree\EntitreeManagerInterface
   */
  protected $entitreeManager;

  /**
   * EntitreeDisplayController constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager service.
   * @param \Drupal\entitree\EntitreeManagerInterface $entitree_manager
   *   The entitree manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntitreeManagerInterface $entitree_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entitreeManager = $entitree_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entitree.manager')
    );
  }

  /**
   * Render the tree display starting with the given parent.
   *
   * @param \Drupal\entitree\EntitreeLocationInterface $parent
   *   The parent location to render the tree from.
   * @param array $destination
   *   An array identifying routes for each link to render.
   * @param string $pageHelp
   *   Help text to display with the tree.
   * @param \Drupal\entitree\EntitreeLocationInterface $activeLocation
   *   The active location if relevant.
   *
   * @return array
   *   A render array for the tree display.
   */
  public function renderTreeDisplay(EntitreeLocationInterface $parent = NULL, array $destination = NULL, string $pageHelp = "", EntitreeLocationInterface $activeLocation = NULL): array {
    /*
     * If a parent location was not provided in the path use the root location
     * as the parent.
     */
    if (!$parent) {
      $parent = \Drupal::service('entitree.manager')->getSiteRootLocation(\Drupal::languageManager()->getCurrentLanguage()->getId());
    }

    if (!$parent) {
      return [
        "#markup" => "No root entity has been created yet. You can start by setting the homepage to an entity path",
      ];
    }

    // Prepare the routes used when a location is clicked to browse.
    if (!$destination) {
      $destination = [
        "browse" => [
          "route" => "entitree.entitree_display_controller_RenderTreeDisplay",
          "parameters" => [],
        ],
        "manage_permissions" => [
          "route" => "entitree_permissions.location_permissions_controller_render",
          "parameters" => [],
        ],
      ];
    }

    // Get children to render for this parent.
    $children = $parent->getChildren();

    // Load the ancestors of the parent location.
    $ancestors = $parent->getAncestors();

    return [
      '#theme' => 'entitree__tree_display',
      '#parent' => $parent,
      '#ancestors' => $ancestors,
      '#children' => $children,
      '#destination' => $destination,
      '#pageHelp' => $pageHelp,
      '#activeLocation' => $activeLocation,
      '#attached' => [
        'library' => [
          'entitree/entitree',
        ],
      ],
    ];
  }

  /**
   * Wrapper for the EmptyLocationEntity form to prepopulate entity values.
   *
   * @param \Drupal\entitree\EntitreeLocationInterface $parent
   *   The parent location in the Entitree structure.
   */
  public function renderAddEmptyLocationForm(EntitreeLocationInterface $parent) {
    // Create an empty location.
    $emptyLocation = EmptyLocation::create(['label' => ""]);
    $emptyLocation->save();

    $entity = EntitreeLocation::create([
      'ref_entity_type' => 'empty_location',
      'ref_entity_id' => $emptyLocation->id(),
      'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
      'parent' => $parent,
    ]);

    /*
     * TODO add cron task to clean up empty locations that don't have associated
     * entitree location entities.
     */

    // Build form for this location entity.
    return $this->entityFormBuilder()->getForm($entity, 'add', ["parent" => $parent]);
  }

  /**
   * Render the tree display with logic to direct user to add form upon select.
   *
   * @param string $entityType
   *   The entity type id string.
   * @param int $entityId
   *   The ID of the entity receiving the location.
   * @param \Drupal\entitree\EntitreeLocationInterface $parent
   *   The parent location if the user has already browsed the tree.
   */
  public function renderTreeAndAdd(string $entityType, int $entityId, EntitreeLocationInterface $parent = NULL) {
    // Prepare destination options.
    $destination = [
      "browse" => [
        "route" => "entitree.entitree_display_controller_renderTreeAndAdd",
        "parameters" => [
          "entityType" => $entityType,
          "entityId" => $entityId,
        ],
      ],
      "select" => [
        "route" => "entitree.location.add",
        "parameters" => [
          "entity_type" => $entityType,
          "entity_id" => $entityId,
        ],
      ],
    ];

    $pageHelp = $this->t('Browse the Entitree structure by clicking on location names. To select a location, click on the "S" icon next to the location name.');

    return $this->renderTreeDisplay($parent, $destination, $pageHelp);
  }

  /**
   * Generate and render the add location form.
   *
   * @param string $entity_type
   *   The entity type ID string.
   * @param int $entity_id
   *   The entity ID.
   * @param \Drupal\entitree\EntitreeLocationInterface $parent
   *   The parent of the new location.
   *
   * @return array
   *   The render array.
   */
  public function renderAddLocationForm(string $entity_type, int $entity_id, EntitreeLocationInterface $parent): array {
    // Create entity based on url parameters.
    $entity = EntitreeLocation::create([
      'ref_entity_type' => $entity_type,
      'ref_entity_id' => $entity_id,
      'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
    ]);

    // Build form for this location entity.
    return $this->entityFormBuilder()->getForm($entity, 'add', ['parent' => $parent]);
  }

  /**
   * Render the tree browser display and set the destination to the edit form.
   *
   * @todo currently on the location edit form parent can be null because the
   * root location doesn't have a parent. This needs to be revisited. If it can
   * be null, it needs to be the second parameter here.
   *
   * @param \Drupal\entitree\EntitreeLocationInterface $parent
   *   The parent location for the tree display.
   * @param \Drupal\entitree\EntitreeLocationInterface $location
   *   The location to edit after selecting a parent.
   *
   * @return array
   *   The render array.
   */
  public function renderTreeAndEdit(
    EntitreeLocationInterface $parent,
    EntitreeLocationInterface $location
  ): array {
    // Prepare destination.
    $destination = [
      "browse" => [
        "route" => "entitree.entitree_display_controller_renderTreeAndEdit",
        "parameters" => [
          "location" => $location->getId(),
        ],
      ],
      "select" => [
        "route" => "entitree.location.edit",
        "parameters" => [
          "location" => $location->getId(),
        ],
      ],
    ];

    $pageHelp = $this->t('Browse the Entitree structure by clicking on location names. To select a location, click on the "S" icon next to the location name.');

    return $this->renderTreeDisplay($parent, $destination, $pageHelp, $location);
  }

  /**
   * Render table of entity's Entitree locations.
   */
  public function renderEntityLocations() {
    // Load all route parameters.
    $params = \Drupal::request()->attributes->get('_route_params');

    /*
     * Use the entitree entity type plugin to load the appropriate entity using
     * the entity_type param.
     */
    $entitreeEntityTypeManager = \Drupal::service('plugin.manager.entitree_entity_type');
    $plugin = $entitreeEntityTypeManager->getEntitreeEntityType($params['entity_type']);

    // Use the plugin to load the correct entity object.
    $entity = $plugin->loadEntityByRouteParams($params);

    // Load all orphaned path aliases.
    /** @var \Drupal\path_alias\PathAliasInterface[] $orphanedPathAliases */
    // $orphanedPathAliases = $plugin::getOrphanedpathAliases($entity->id());

    // Load the locations for this entity.
    $locations = $this->entitreeManager->getLocationsByIdAndType(
      $entity->id(),
      $params['entity_type']
    );

    // Load the ancestors for each location.
    foreach ($locations as $i => $location) {
      $locations[$i]->ancestors = $location->getAncestors();
    }

    return [
      "#theme" => 'entitree__manage_locations',
      '#locations' => $locations,
      '#attached' => [
        'library' => [
          'entitree/entitree',
        ],
      ],
    ];
  }

}
