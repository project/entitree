<?php

namespace Drupal\entitree\Event;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event definition for the entitree operations event.
 */
class EntitreeOperationsEvent extends Event {
  const EVENT_NAME = 'entitree_operations';

  /**
   * The operations defined when the event is created.
   *
   * @var array
   */
  protected $initialOperations;

  /**
   * The current list of operation definitions.
   *
   * @var array
   */
  protected $operations;

  /**
   * {@inheritDoc}
   */
  public function __construct(array $initialOperations) {
    $this->initialOperations = $this->operations = $initialOperations;
  }

  /**
   * Get the operation definitions without any pending modifications.
   *
   * @return array
   *   Array of initial operation definitions.
   */
  public function getInitialOperations(): array {
    return $this->initialOperations;
  }

  /**
   * Get the current array of operation definitions.
   *
   * @return array
   *   Array of current operation definitions.
   */
  public function getOperations(): array {
    return $this->operations;
  }

  /**
   * Set a new list of operations.
   *
   * @param array $operations
   *   Array of operation definitions.
   *
   * @return EntitreeOperationsEvent
   *   The updated instance of the EntitreeOperationsEvent object.
   */
  public function setOperations(array $operations): EntitreeOperationsEvent {
    $this->operations = $operations;
    return $this;
  }

}
