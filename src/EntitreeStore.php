<?php

namespace Drupal\entitree;

use Drupal\entitree\Entity\EntitreeLocation;
use Drupal\entitree\NestedSetFactory;

/**
 * Provides integration between entities and Entitree storage.
 */
class EntitreeStore implements EntitreeStoreInterface {
  /**
   * Key=>value index of all loaded Entitree locations.
   *
   * @var array
   *
   * Key=>Value index where key is an EntitreeLocation ID and the value is an
   * array containing the parent EntitreeLocation ID and the location nested set
   * node ID.
   *
   * @todo This is cached
   */
  protected $index = [];

  /**
   * Parent-indexed array of Entitree locations.
   *
   * @var array
   *
   * Generated index to make children lookups faster. Key is parent
   * EntitreeLocation ID and value is array of children EntitreeLocation IDs.
   *
   * This is not cached
   */
  protected $indexReversed = [];

  /**
   * Two letter langcode.
   *
   * @var string
   */
  protected $langcode;

  /**
   * Instance of global EntitreeManager service.
   *
   * @var EntitreeManagerInterface
   */
  protected $entitreeManager;

  /**
   * @var NestedSetFactory $factory
   */
  protected $factory;

  /**
   * Create instance of EntitreeStore.
   *
   * @param string $langcode
   *   Two letter langcode for this Entitree store.
   * @param EntitreeManagerInterface $entitreeManager
   *   Entitree Manager service.
   */
  public function __construct(
    string $langcode,
    EntitreeManagerInterface $entitreeManager
  ) {
    $this->langcode = $langcode;
    $this->entitreeManager = $entitreeManager;
    $this->factory = $this->entitreeManager->getNestedSetFactory($langcode);
  }

  /**
   * Get the langcode defined on store construction.
   *
   * @return string
   *   Two letter langcode for this Entitree store.
   */
  public function getLangcode(): string {
    return $this->langcode;
  }

  /**
   * Check to see if specified location already exists in this data store.
   *
   * @param int $locationId
   *   ID of the Entitree location.
   *
   * @return bool
   *   TRUE if location is indexed. FALSE if not indexed.
   */
  protected function isIndexed(int $locationId): bool {
    return isset($this->index[$locationId]);
  }

  /**
   * Add an EntitreeLocation to the store index.
   *
   * @param \Drupal\entitree\EntitreeLocationInterface $location
   *   The Entitree location to add to the index.
   */
  protected function addLocationToIndex(EntitreeLocationInterface $location) {
    if (!$this->isIndexed($location->getId())) {
      // Get parent location and continue if not the site root.
      if ($parentLocationId = $this->getLocationParentId($location->getId())) {
        /* Check to see if all ancestors are loaded by looking for the parent
         * in the index.
         */
        if (!$this->isIndexed($parentLocationId)) {
          // Load ancestor locations from database.
          $ancestorRows = $this->loadLocationAncestorIds($location->getId());
          $ancestorRows = array_reverse($ancestorRows);

          // Add each ancestor that doesn't yet exist in the index to the index.
          foreach ($ancestorRows as $index => $row) {
            if ($this->isIndexed($row['location_id'])) {
              break;
            }

            $this->index[$row['location_id']] = [
              // If this is the last row we should be at the site root.
              'parentLocationId' => $index + 1 < count($ancestorRows)
              ? $ancestorRows[$index + 1]['location_id']
              : NULL,
              'nestedSetNodeId' => $row['nested_set_id'],
            ];
          }
        }
      }

      // Add the location ID and its parent ID to the index.
      $this->index[$location->getId()] = [
        'parentLocationId' => $parentLocationId,
        'nestedSetNodeId' => $this->getLocationNestedSetNodeId($location->getId()),
      ];

      // Add the location to the reversed Index.
      $this->indexReversed[$parentLocationId] =
        $this->indexReversed[$parentLocationId] ?? [];
      $this->indexReversed[$parentLocationId][] = $location->getId();
    }
  }

  /**
   * {@inheritDoc}
   */
  public function insertlocation(
    EntitreeLocationInterface $location,
    EntitreeLocationInterface $parent
  ): int {
    // Verify this location has a parent or is the site root.
    if ($parent) {
      $parentNestedSetNodeId = $this->getLocationNestedSetNodeId(
        $parent->getId());
      $nestedSetNodeId = (int) $this->factory->insert($parentNestedSetNodeId,
        ["location_id" => $location->getId()]);
    }
    elseif ($location->isSiteRoot()) {
      $parentNestedSetNodeId = $this->factory->getRootNodeId();
      $nestedSetNodeId = (int) $this->factory->insert($parentNestedSetNodeId,
        ["location_id" => $location->getId()]);
    }
    else {
      throw new \Exception('The location ' . $location->getLabel() . ' could not
       be inserted because it does not have a parent and is not the site root.'
      );
    }

    $this->addLocationToIndex($location);

    return $nestedSetNodeId;
  }

  /**
   * {@inheritDoc}
   */
  public function getLocation(int $locationId): EntitreeLocationInterface {
    if ($this->isIndexed($locationId)) {
      $location = EntitreeLocation::load($locationId);
    }
    else {
      // Load the entity.
      $location = EntitreeLocation::load($locationId);

      // Add the location to the index.
      if (!$this->isIndexed($locationId)) {
        $this->addLocationToIndex($location);
      }
    }

    return $location;
  }

  /**
   * Generate a list of all ancestor IDs.
   *
   * @param int $locationId
   *   The location from where the search begins.
   *
   * @return array
   *   Array of ancestor IDs starting from the site root.
   */
  protected function loadLocationAncestorIds(int $locationId): array {
    $locationNestedSetNodeId = $this->getLocationNestedSetNodeId($locationId);
    $ancestorRows = $this->factory->findAncestors($locationNestedSetNodeId,
      ['id', 'ordering', 'depth', 'location_id'], "depth");

    // Rename id to nested_set_id for clarity.
    foreach ($ancestorRows as &$row) {
      $row['nested_set_id'] = $row['id'];
      unset($row['id']);
    }
    return $ancestorRows;
  }

  /**
   * {@inheritDoc}
   */
  public function getLocationAncestors(int $locationId): array {
    /*
     * Find parent location ID to determine if ancestors are already loaded and
     * indexed.
     */
    $parentLocationId = $this->getLocationParentId($locationId);

    // For the site root, return an empty array.
    if (!$parentLocationId) {
      return [];
    }

    // Start by checking if parent location is already loaded.
    if ($this->isIndexed($parentLocationId)) {
      // Location is loaded, so all ancestors should be indexed already.
      $ancestors = [];
      $siteRootLocationId = $this
        ->entitreeManager
        ->getSiteRootLocation($this->langcode)->getId();

      while (TRUE) {
        /*
         * Insert the next parent at the beginning of the array until reaching
         * the site root node.
         */
        array_unshift($ancestors, EntitreeLocation::load($parentLocationId));

        // If at the site root discontinue the loop.
        if ($parentLocationId == $siteRootLocationId) {
          break;
        }

        $parentLocationId = $this->index[$parentLocationId]['parentLocationId'];
      }
    }
    else {
      // If ancestors are not yet indexed fetch them from the database.
      $ancestorIds = $this->loadLocationAncestorIds($locationId);
      foreach ($ancestorIds as $ancestorRow) {
        $ancestors[] = $this->getLocation($ancestorRow['location_id']);
      }
    }

    return $ancestors;
  }

  /**
   * {@inheritDoc}
   */
  public function getLocationParent(int $locationId): ?EntitreeLocationInterface {
    $ancestors = $this->getLocationAncestors($locationId);
    return array_pop($ancestors);
  }

  /**
   * {@inheritDoc}
   */
  public function getLocationChildren(EntitreeLocationInterface $location): array {
    // Load all children IDs from the database.
    $nestedSetNodeId = $this->getLocationNestedSetNodeId($location->getId());
    $childrenRows = $this
      ->entitreeManager
      ->getNestedSetFactory($this->langcode)
      ->findChildren($nestedSetNodeId, NULL, 'ordering');

    // Load all of the entitites.
    $children = [];
    foreach ($childrenRows as $row) {
      $children[] = $this->getLocation($row['location_id']);
    }

    return $children;
  }

  /**
   * Load all descendant IDs using factory.
   *
   * @param int $locationId
   *   Entitree Location ID.
   *
   * @return array
   *   Array of nested set rows.
   */
  protected function getLocationDescendantIds(int $locationId): array {
    $locationNestedSetNodeId = $this->getLocationNestedSetNodeId($locationId);
    $descendantRows = $this
      ->factory
      ->findDescendants($locationNestedSetNodeId,
        ['id', 'ordering', 'depth', 'location_id'], "depth, ordering");

    // Rename id to nested_set_id for clarity.
    foreach ($descendantRows as &$row) {
      $row['nested_set_id'] = $row['id'];
      unset($row['id']);
    }
    return $descendantRows;
  }

  /**
   * {@inheritDoc}
   */
  public function getLocationDescendants(int $locationId): array {
    $descendantRows = $this->getLocationDescendantIds($locationId);

    $descendants = [];

    // Load each location and add it to the index if necessary.
    foreach ($descendantRows as $descendantRow) {
      $descendant = EntitreeLocation::load($descendantRow['location_id']);

      if (!$this->isIndexed($descendantRow['location_id'])) {
        $this->addLocationToIndex($descendant);
      }

      $descendants[] = $descendant;
    }

    return $descendants;
  }

  /**
   * Fetch parent ID from index or database.
   *
   * @param int $locationId
   *   Entitree Location ID.
   *
   * @return int
   *   Entitree Location ID of parent.
   */
  protected function getLocationParentId(int $locationId): ?int {
    // Return parent location ID from index if available.
    if ($this->isIndexed($locationId)) {
      return $this->index[$locationId]['parentLocationId'];
    }

    // Load parent location ID from database.
    $childNestedSetNodeId = $this->getLocationNestedSetNodeId($locationId);

    $result = $this
      ->entitreeManager
      ->getNestedSetFactory($this->langcode)
      ->findParent($childNestedSetNodeId);

    return $result ? $result['location_id'] : NULL;
  }

  /**
   * Fetch the nested set node ID using EntitreeLocation ID.
   *
   * @param int $locationId
   *   Entitree Location ID.
   *
   * @return int
   *   ID of the node in the nested set tree.
   */
  protected function getLocationNestedSetNodeId(int $locationId): int {
    // Check store for nested set node ID.
    if ($this->isIndexed($locationId)) {
      return (int) $this->index[$locationId]['nestedSetNodeId'];
    }
    else {
      $nestedSetNodeId = $this->factory->findNestedSetIdByLocationId($locationId);
      if (!is_null($nestedSetNodeId)) {
        return $nestedSetNodeId;
      }
      else {
        throw new \Exception('No corresponding nested set node could be found
        for this location');
      }

    }
  }

  /**
   * {@inheritDoc}
   */
  public function moveLocation(int $locationId, int $newParentId) {
    $this->factory->move($this->getLocationNestedSetNodeId($locationId),
      $this->getLocationNestedSetNodeId($newParentId));
  }

  /**
   * {@inheritDoc}
   */
  public function removeLocationAndDescendants(int $locationId) {
    // Load all descendant ids in order to remove them from the index.
    $descendants = $this->getLocationDescendantIds($locationId);

    // Remove all descendants from index.
    foreach ($descendants as $descendant) {
      if (isset($this->index[$descendant['location_id']])) {
        unset($this->index[$descendant['location_id']]);
      }
    }

    // Remove the top level location from index.
    if (isset($this->index[$locationId])) {
      unset($this->index[$locationId]);
    }

    // Remove from nested set structure.
    $this->factory->delete($this->getLocationNestedSetNodeId($locationId));
  }

}
