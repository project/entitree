<?php

namespace Drupal\entitree;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\path_alias\AliasManagerInterface;

/**
 * Provides a utility for creating a unique path alias.
 *
 * This is a modified version of AliasUniquifier class in pathauto:8.x-3.1.8.
 */
class AliasUniquifier implements AliasUniquifierInterface {

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The route provider service.
   *
   * @var \Drupal\Core\Routing\RouteProviderInterface
   */
  protected $routeProvider;

  /**
   * The alias manager.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * The maximum length a path can be.
   *
   * @var int
   */
  protected static $maxPathLength = 255;

  /**
   * The separator to use for generated path segments.
   *
   * @var string
   */
  protected static $pathSeparator = '-';

  /**
   * Creates a new AliasUniquifier.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   *   The alias storage helper.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Routing\RouteProviderInterface $route_provider
   *   The route provider service.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   The alias manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler, RouteProviderInterface $route_provider, AliasManagerInterface $alias_manager) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->routeProvider = $route_provider;
    $this->aliasManager = $alias_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function uniquify($alias, $source, $langcode):string {
    if (!$this->isReserved($alias, $source, $langcode)) {
      $pathArray = explode('/', $alias);
      return end($pathArray);
    }

    // If the alias already exists, generate a new, hopefully unique, variant.
    $maxlength = self::$maxPathLength;
    $separator = self::$pathSeparator;
    $original_alias = $alias;

    $i = 0;
    do {
      // Append an incrementing numeric suffix until we find a unique alias.
      $unique_suffix = $separator . $i;
      $alias = Unicode::truncate($original_alias, $maxlength - mb_strlen($unique_suffix), TRUE) . $unique_suffix;
      $i++;
    } while ($this->isReserved($alias, $source, $langcode));

    $pathArray = explode('/', $alias);
    return end($pathArray);
  }

  /**
   * {@inheritdoc}
   */
  public function isReserved($alias, $source, $langcode = LanguageInterface::LANGCODE_NOT_SPECIFIED) {
    // Check if this alias already exists.
    if ($existing_source = $this->aliasManager->getPathByAlias($alias, $langcode)) {
      if ($existing_source != $alias) {
        /*
         * If it is an alias for the provided source, it is allowed to keep
         * using it. If not, then it is reserved.
         */
        return $existing_source != $source;
      }
    }

    // Then check if there is a route with the same path.
    if ($this->isRoute($alias)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Verify if the given path is a valid route.
   *
   * @param string $path
   *   A string containing a relative path.
   *
   * @return bool
   *   TRUE if the path already exists.
   *
   * @throws \InvalidArgumentException
   */
  public function isRoute($path) {
    if (is_file(DRUPAL_ROOT . '/' . $path) || is_dir(DRUPAL_ROOT . '/' . $path)) {
      // Do not allow existing files or directories to get assigned an automatic
      // alias. Note that we do not need to use is_link() to check for symbolic
      // links since this returns TRUE for either is_file() or is_dir() already.
      return TRUE;
    }

    $routes = $this->routeProvider->getRoutesByPattern($path);

    // Only return true for an exact match, ignore placeholders.
    foreach ($routes as $route) {
      if ($route->getPath() == $path) {
        return TRUE;
      }
    }

    return FALSE;

  }

}
