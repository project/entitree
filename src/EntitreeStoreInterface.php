<?php

namespace Drupal\entitree;

/**
 * Interface for Entitree Store class definitions.
 */
interface EntitreeStoreInterface {

  /**
   * Get the langcode value for this store.
   *
   * @return string
   *   2-letter langcode string.
   */
  public function getLangcode(): string;

  /**
   * Load location from index or have it added before returning location.
   *
   * @param int $locationId
   *   ID of the Entitree Location.
   *
   * @return EntitreeLocationInterface
   *   Entitree Location entity.
   */
  public function getLocation(int $locationId): EntitreeLocationInterface;

  /**
   * Return array of ancestors with the site root as the first element.
   *
   * @param int $locationId
   *   ID of location to begin search.
   *
   * @return array
   *   Array of ancestors in order of depth beginning with site root.
   */
  public function getLocationAncestors(int $locationId): array;

  /**
   * Fetch the children from the nested set tree.
   *
   * @param EntitreeLocationInterface $location
   *   Entitree location entity.
   *
   * @return EntitreeLocationInterface[]
   *   Array of Entitree location entities.
   */
  public function getLocationChildren(EntitreeLocationInterface $location): array;

  /**
   * Get all descendant entitree locations of a provided location.
   *
   * @param int $locationId
   *   Entitree Location ID.
   *
   * @return array
   *   Array of Entitree Location entities.
   */
  public function getLocationDescendants(int $locationId): array;

  /**
   * Move a location to another location within the nested set tree.
   *
   * @param int $locationId
   *   Entitree Location ID to move.
   * @param int $newParentId
   *   Entitree Location ID of parent destination.
   */
  public function moveLocation(int $locationId, int $newParentId);

  /**
   * Remove location and all of its descendants.
   *
   * @param int $locationId
   *   Entitree Location ID to remove.
   */
  public function removeLocationAndDescendants(int $locationId);

  /**
   * Inserts location into the nested set table and adds it to the store index.
   *
   * @param \Drupal\entitree\EntitreeLocationInterface $location
   *   Entitree location to add to table.
   * @param \Drupal\entitree\EntitreeLocationInterface $parent
   *   Entitree location to use as the parent.
   *
   * @return int
   *   The ID in the tree structure associated with this location.
   */
  public function insertlocation(
    EntitreeLocationInterface $location,
    EntitreeLocationInterface $parent
  ): int;

  /**
   * Load the parent of a location.
   *
   * @param int $locationId
   *   The ID of the child Entitree Location.
   *
   * @return \Drupal\entitree\EntitreeLocationInterface|null
   *   The parent Entitree location if one exists.
   */
  public function getLocationParent(int $locationId): ?EntitreeLocationInterface;

}
