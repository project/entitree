<?php

namespace Drupal\entitree\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;

/**
 * Defines dynamic local tasks.
 */
class DynamicTasks extends DeriverBase {

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    $entitreeTypesManager = \Drupal::service('plugin.manager.entitree_entity_type');

    // For each entitree entity type plugin get the appropriate local tasks.
    foreach ($entitreeTypesManager->getDefinitions() as $typeDefinition) {
      // Foreach enabled type plugin get an instance of the class.
      $plugin = $entitreeTypesManager->getEntitreeEntityType($typeDefinition['id']);

      $tasks = $plugin->getLocalTasks($base_plugin_definition);

      foreach ($tasks as $key => $task) {
        $this->derivatives[$key] = $task;
      }
    }

    return $this->derivatives;
  }

}
