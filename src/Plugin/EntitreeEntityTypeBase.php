<?php

namespace Drupal\entitree\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Entitree entity type plugins.
 */
abstract class EntitreeEntityTypeBase extends PluginBase implements EntitreeEntityTypeInterface {

  /**
   * {@inheritDoc}
   */
  public static function applyTokenReplaceActions(string $value, $entity): string {
    return $value;
  }

  /**
   * {@inheritDoc}
   */
  public function getOperations(): array {
    return [
      'view' => [
        'weight' => 10,
      ],
      'edit' => [
        'weight' => 20,
      ],
      'manageLocations' => [
        'weight' => 30,
      ],
      'delete' => [
        'weight' => 40,
      ],
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getLocalTasks($base_plugin_definition): array {
    $tasks = [];

    return $tasks;
  }

  /**
   * {@inheritDoc}
   */
  public static function getTokenTypes(): array {
    return [];
  }

}
