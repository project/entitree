<?php

namespace Drupal\entitree\Plugin\EntitreeEntityType;

use Drupal\entitree\Plugin\EntitreeEntityTypeBase;
use Symfony\Component\Routing\Route;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\entitree\Entity\EmptyLocation;

/**
 * Provides entitree support for the custom empty_location entity type.
 *
 * @EntitreeEntityType(
 *   id = "empty_location",
 *   entity_id = "empty_location",
 *   entity_name = "Empty Location"
 * )
 */
class EntitreeEmptyLocation extends EntitreeEntityTypeBase {

  /**
   * {@inheritDoc}
   */
  public function buildManageLocationRoute(array &$routes) {
    $route = (new Route('/admin/structure/entitree/{empty_location}/locations/{entity_type}'))
      ->addDefaults([
        '_controller' => '\Drupal\entitree\Controller\EntitreeDisplayController::renderEntityLocations',
        '_title' => 'Manage Locations',
        'entity_type' => 'empty_location',
      ])
      ->setRequirement('empty_location', '\d+')
      // TODO setup correct permissions.
      ->setRequirement('_permission', 'access content')
      ->setOption('parameters', [
        'emtpy_location' => [
          "type" => "EmptyLocation",
        ],
      ]);
    $routes['entitree.locations.empty_location'] = $route;
  }

  /**
   * {@inheritDoc}
   */
  public function getSourcePath($id): string {
    return "/location/$id";
  }

  /**
   * {@inheritDoc}
   */
  public function loadEntityByRouteParams(array $params): EntityInterface {
    return EmptyLocation::load($params['empty_location']);
  }

  /**
   * {@inheritDoc}
   */
  public function getLocationsPathById(int $id): Url {
    return Url::fromRoute('entitree.locations.empty_location', [
      "empty_location" => $id,
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public static function getExistingPathAliases(int $id): array {
    /** @var \Drupal\Core\Entity\EntityStorageBaseInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('path_alias');
    $result = $storage->loadByProperties(['path' => "/location/$id"]);

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public static function getOrphanedpathAliases(int $id): array {
    /** @var \Drupal\path_alias\PathAliasInterface[] $pathAliases */
    $pathAliases = self::getExistingPathAliases($id);

    // Determine which of these have locations for this empty location.
    /** @var \Drupal\entitree\EntitreeManagerInterface $entitreeManager */
    $entitreeManager = \Drupal::service('entitree.manager');
    $locations = $entitreeManager->getLocationsByIdAndType($id, 'empty_location');

    $orphaned = [];
    foreach ($pathAliases as $alias) {
      // Search locations for one associated with this path alias ID.
      foreach ($locations as $location) {
        if ($location->get('pid')->getString() == $alias->id()) {
          continue 2;
        }
      }

      /*
       * If reaching this point the alias does not have a corresponding
       * location.
       */
      $orphaned[] = $alias;
    }

    return $orphaned;
  }

}
