<?php

namespace Drupal\entitree\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines an interface for Entitree entity type plugins.
 */
interface EntitreeEntityTypeInterface extends PluginInspectionInterface {

  /**
   * Defines the route to manage Entitree locations for this entity type.
   *
   * @param array $routes
   *   Array of routes to append to or modify.
   */
  public function buildManageLocationRoute(array &$routes);

  /**
   * Apply token replacements to a string using values from the provided entity.
   *
   * @param string $value
   *   The string value containing tokens.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity context.
   *
   * @return string
   *   The updated string value.
   */
  public static function applyTokenReplaceActions(string $value, EntityInterface $entity): string;

  /**
   * Defines the source path to use when generating URL Aliases.
   *
   * @param int|string $id
   *   The ID of the entity.
   *
   * @return string
   *   The source path for the entity.
   */
  public function getSourcePath($id): string;

  /**
   * Load an entity by its route parameters.
   *
   * @param array $params
   *   The route parameters.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The Entity matching the route parameters.
   */
  public function loadEntityByRouteParams(array $params): EntityInterface;

  /**
   * Get the URL to the Manage Locations route for a specific entity.
   *
   * @param int $entity_id
   *   The ID of the entity.
   *
   * @return \Drupal\Core\Url
   *   The URL of the manage locations route.
   */
  public function getLocationsPathById(int $entity_id): Url;

  /**
   * Get the local tasks for this entity type.
   *
   * @param array|\Drupal\Component\Plugin\Definition\PluginDefinitionInterface $base_plugin_definition
   *   The definition of the base plugin from which the derivative plugin
   *   is derived. It is maybe an entire object or just some array, depending
   *   on the discovery mechanism.
   *
   * @return array
   *   An array of local tasks specific to this entity type.
   */
  public function getLocalTasks($base_plugin_definition): array;

  /**
   * Provide a list of default operations that may be used by type plugins.
   *
   * @return array
   *   Array of operations.
   */
  public function getOperations(): array;

  /**
   * Provide an array of token types available for this entity type.
   *
   * @return array
   *   Array of token types.
   */
  public static function getTokenTypes(): array;

  /**
   * Load all path aliases for this Entity type.
   *
   * @param int $id
   *   ID of the entity to restrict the search to paths related.
   *
   * @return \Drupal\path_alias\PathAliasInterface[]
   *   Array of rows from the path_alias table.
   */
  public static function getExistingPathAliases(int $id): array;

  /**
   * Get all path aliases that do not correspond to an Entitree location.
   *
   * @param int $id
   *   Entity ID associated with the path aliases.
   *
   * @return array
   *   Array of rows from the path_alias table.
   */
  public static function getOrphanedpathAliases(int $id): array;

}
