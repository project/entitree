<?php

namespace Drupal\entitree\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Entitree entity type plugin manager.
 */
class EntitreeEntityTypeManager extends DefaultPluginManager {

  /**
   * Constructs a new EntitreeEntityTypeManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/EntitreeEntityType', $namespaces, $module_handler, 'Drupal\entitree\Plugin\EntitreeEntityTypeInterface', 'Drupal\entitree\Annotation\EntitreeEntityType');

    $this->alterInfo('entitree_entitree_entity_type_info');
    $this->setCacheBackend($cache_backend, 'entitree_entitree_entity_type_plugins');
  }

  /**
   * Get an instance of the plugin given an entity type.
   *
   * @param string $entityTypeId
   *   ID of the entity type plugin to load.
   *
   * @return EntitreeEntityTypeInterface
   *   The plugin instance matching the entity type.
   */
  public function getEntitreeEntityType(string $entityTypeId): EntitreeEntityTypeInterface {
    return $this->createInstance($entityTypeId);
  }

}
