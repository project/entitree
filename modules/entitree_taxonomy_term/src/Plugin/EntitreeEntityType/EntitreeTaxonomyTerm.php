<?php

namespace Drupal\entitree_taxonomy_term\Plugin\EntitreeEntityType;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\Entity;
use Drupal\entitree\Plugin\EntitreeEntityTypeBase;
use Symfony\Component\Routing\Route;

/**
 * Provides entitree support for taxonomy terms
 *
 * @EntitreeEntityType(
 *   id = "taxonomy_term",
 *   entity_id = "taxonomy_term",
 *   entity_name = "Taxonomy Term"
 * )
 */
class EntitreeTaxonomyTerm extends EntitreeEntityTypeBase {
  public function buildManageLocationRoute(array &$routes) {
    /**
     * @todo this throws an error with the devel module because it seems to have
     * a route that requires the first parameter here to be named {node}. We
     * need it to be named something generic as this controller is used for all
     * entity types. This seems like a problem with the devel module but we may
     * need to find a workaround
     */
    $route = (
      new Route('/taxonomy/term/{taxonomy_term}/locations/{entity_type}'))
        ->addDefaults([
          '_controller' =>
'\Drupal\entitree\Controller\EntitreeDisplayController::renderEntityLocations',
          '_title' => 'Manage Locations',
          'entity_type' => 'taxonomy_term',
      ])
      ->setRequirement('taxonomy_term', '\d+')
      /**
       * @todo setup permissions using logic that checks the manage locations
       * permissions
       */
      ->setRequirement('_permission', 'administer entitree')
      ->setOption('_admin_route', TRUE);
    $routes['entitree.locations.taxonomy_term'] = $route;
  }

  public function getSourcePath($id): string {
    return "/taxonomy/term/" . $id;
  }

  public function loadEntityByRouteParams(
    array $params
  ): \Drupal\Core\Entity\EntityInterface {
    return \Drupal\taxonomy\Entity\Term::load($params['taxonomy_term']);
  }

  public function getLocationsPathById(int $tid): \Drupal\Core\Url{
    return \Drupal\Core\Url::fromRoute('entitree.locations.taxonomy_term', [
      "taxonomy_term" => $tid
    ]);
  }

  public function getLocalTasks($base_plugin_definition): array {
    $tasks = [];

    $tasks['entitree.entity_edit_display'] = $base_plugin_definition;
    $tasks['entitree.entity_edit_display']['title'] = 'Manage Locations';
    $tasks['entitree.entity_edit_display']['route_name'] =
      'entitree.locations.taxonomy_term';
    $tasks['entitree.entity_edit_display']['base_route'] =
      'entity.taxonomy_term.canonical';

    return $tasks;
  }

  /**
   * {@inheritDoc}
   */
  public static function getTokenTypes(): array {
    return ['term'];
  }

  /**
   * Apply token replace action to a string using values from the provided entity.
   *
   * @param string $value
   * @param \Drupal\taxonomy\TermInterface $entity
   * @return string
   */
  public static function applyTokenReplaceActions(string $value, $entity): string
  {
    return \Drupal::service('token')->replace($value, [
      'term' => $entity
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public static function getExistingPathAliases(int $id): array {
    /** @var \Drupal\Core\Entity\EntityStorageBaseInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('path_alias');
    $result = $storage->loadByProperties(['path' => "/taxonomy/term/$id"]);

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public static function getOrphanedpathAliases(int $id): array {
    /** @var \Drupal\path_alias\PathAliasInterface[] $pathAliases */
    $pathAliases = self::getExistingPathAliases($id);

    // Determine which of these have locations for this taxonomy term.
    /** @var \Drupal\entitree\EntitreeManagerInterface $entitreeManager */
    $entitreeManager = \Drupal::service('entitree.manager');
    $locations = $entitreeManager->getLocationsByIdAndType($id, 'term');

    $orphaned = [];
    foreach ($pathAliases as $alias) {
      // Search locations for one associated with this path alias ID.
      foreach ($locations as $location) {
        if ($location->get('pid')->getString() == $alias->id()) {
          continue 2;
        }
      }

      // If reaching this point the alias doesn't have a corresponding location.
      $orphaned[] = $alias;
    }

    return $orphaned;
  }

}
