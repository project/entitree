<?php

namespace Drupal\entitree_permissions;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class RoleAccessForm.
 */
class EntitreePermissionsFormBase extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entitree_permissions_role_access';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    array $permission = null
  ) {
    $form_state->setStorage(['permission' => $permission]);

    $form['access_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Access Type'),
      '#options' => [
        '' => '',
        'allowed' => 'Allowed',
        'denied' => 'Denied'
      ],
      '#required' => TRUE,
      '#weight' => 70,
      '#default_value' => $permission && isset($permission['access_type']) ? $permission['access_type'] : ''
    ];

    $form['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operation'),
      '#options' => call_user_func(function(){
        $currentRoute = \Drupal::routeMatch();
        $location = $currentRoute->getParameters()->get('location');
        $operations = \Drupal::entityTypeManager()
          ->getListBuilder($location->getReferencedEntityType())
          ->getOperations($location->getReferencedEntity());

        $options = [
          '' => '',
          /**
           * View is not provided by EntityListBuilder::getOperations so we add
           * it manually
           */
          'view' => $this->t('View')
        ];

        foreach($operations as $key=>$operation){
          $options[$key] = $operation['title'];
        }

        return $options;
      }),
      '#required' => TRUE,
      '#weight' => 80,
      '#default_value' => $permission && isset($permission['operation']) ? $permission['operation'] : ''
    ];

    $form['apply_to_descendants'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Apply to descendents'),
      '#weight' => 90,
      '#default_value' => $permission && isset($permission['apply_to_descendants']) ? $permission['apply_to_descendants'] === '1' : FALSE
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#weight' => 100
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  protected function storePermissionData(
    string $type,
    string $value,
    FormStateInterface $form_state
  ) {
    $storage = $form_state->getStorage();
    $permission = $storage['permission'];

    $values = $form_state->getValues();
    $accessType = $values['access_type'];
    $operation = $values['operation'];
    $applyToDescendants = $values['apply_to_descendants'];

    $currentRoute = \Drupal::routeMatch();
    $location = $currentRoute->getParameters()->get('location');

    $connection = \Drupal::database();
    $messenger = \Drupal::messenger();

    /**
     * If the permission array is provided then this is an update operation
     */
    if($permission && isset($permission['id'])){
      $connection->update('entitree_permissions')
        ->fields([
          'type' => $type,
          'value' => $value,
          'access_type' => $accessType,
          'operation' => $operation,
          'location' => $location->getId(),
          'apply_to_descendants' => $applyToDescendants
        ])
        ->condition('id', $permission['id'], '=')
        ->execute();

      $messenger->addMessage(
        $this->t('The access has been updated'),
        $messenger::TYPE_STATUS
      );
    }
    else{
      $connection->insert('entitree_permissions')
        ->fields([
          'type' => $type,
          'value' => $value,
          'access_type' => $accessType,
          'operation' => $operation,
          'location' => $location->getId(),
          'apply_to_descendants' => $applyToDescendants
        ])
        ->execute();

      $messenger->addMessage(
        $this->t('The new access has been saved'),
        $messenger::TYPE_STATUS
      );
    }

    // Rebuild the route cache to enforce new permissions
    // TODO see if this can be targeted to only impact the specific subtree.
    \Drupal::service('router.builder')->rebuild();
  }
}
