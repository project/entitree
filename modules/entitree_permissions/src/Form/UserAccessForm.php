<?php

namespace Drupal\entitree_permissions\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entitree_permissions\EntitreePermissionsFormBase;
use Drupal\user\Entity\User;

/**
 * Class UserAccessForm.
 */
class UserAccessForm extends EntitreePermissionsFormBase
{


  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'entitree_permissions_user_access';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    array $permission = null
  ) {
    $form = parent::buildForm($form, $form_state, $permission);

    $form['user'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'user',
      '#title' => $this->t('User'),
      '#description' => $this->t('Begin typing in a username to search for a specific user'),
      '#weight' => '0',
      '#required' => TRUE,
      '#default_value' => $permission && isset($permission['value']) ? User::load($permission['value']) : null
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();

    $this->storePermissionData(
      'user',
      $values['user'],
      $form_state
    );
  }
}
