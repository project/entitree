<?php

namespace Drupal\entitree_permissions\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\entitree_permissions\EntitreePermissionsFormBase;

/**
 * Class RoleAccessForm.
 */
class RoleAccessForm extends EntitreePermissionsFormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entitree_permissions_role_access';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state,
    array $permission = null
  ) {
    $form = parent::buildForm($form, $form_state, $permission);

    $form['role'] = [
      '#type' => 'select',
      '#title' => $this->t('Role'),
      '#options' => call_user_func(function(){
        $roles = user_roles();
        $options = ["" => ""];
        foreach($roles as $key=>$role){
          $options[$key] = $role->label();
        }
        return $options;
      }),
      '#size' => 1,
      '#weight' => '0',
      '#required' => TRUE,
      '#default_value' => $permission && isset($permission['value']) ? $permission['value'] : ''
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $this->storePermissionData(
      'role',
      $values['role'],
      $form_state
    );
  }

}
