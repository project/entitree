<?php

namespace Drupal\entitree_permissions\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entitree\EntitreeLocationInterface;

class EntitreePermissionsRuleDeleteForm extends ConfirmFormBase
{

  /**
   * ID of the rule to delete.
   *
   * @var int
   */
  protected $id;

  protected $accessRuleId;
  protected $location;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntitreeLocationInterface $location = null, int $accessRuleId = NULL)
  {
    $form_state->setStorage([
      'accessRuleId' => $accessRuleId,
      'location' => $location
    ]);

    $this->accessRuleId = $accessRuleId;
    $this->location = $location;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $storage = $form_state->getStorage();

    // Delete the access rule
    $connection = \Drupal::database();
    $count = $connection
      ->delete('entitree_permissions')
      ->condition('id', $storage['accessRuleId'], '=')
      ->execute();

    $messenger = \Drupal::messenger();
    if($count > 0){
      $messenger->addMessage(
        'The access rule was successfully deleted',
        $messenger::TYPE_STATUS
      );
    }
    else{
      $messenger->addMessage(
        'An error occurred while deleting the access rule',
        $messenger::TYPE_ERROR
      );
    }

    $form_state->setRedirect(
      'entitree_permissions.location_permissions_controller_render',
      [
        'location' => $storage['location']->id()
      ]
    );


  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string
  {
    return "entitree_permissions_rule_delete_form";
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl()
  {
    // Determine location associated with this rule

    return new Url(
      'entitree_permissions.location_permissions_controller_render',
      [ 'location' => $this->location->id() ]
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion()
  {
    return t('Are you sure you want to delete this access rule for location %location?', ['%location' => $this->location->label->getString()]);
  }
}
