<?php

namespace Drupal\entitree_permissions\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\entitree\EntitreeLocationInterface;

class EntitreePermissionsLocationAccessRulesForm extends FormBase{
  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'entitree_permissions_location_access_rules';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, EntitreeLocationInterface $location = null, array $accessList = null, array $permissionTypes = null)
  {

    $permissionsManager = \Drupal::service('entitree_permissions.manager');

    $form['addRule'] = [
      '#type' => 'fieldset',
      '#title' => t('Add a new access rule')
    ];
    $form['addRule']['button'] = [
      '#help' => t('Add new access rule'),
      '#type' => 'operations',
      '#links' => []
    ];

    foreach($permissionTypes as $type){
      $form['addRule']['button']['#links'][$type['id']] = [
        'title' => t($type['label']),
        'url' => Url::fromRoute($type['addRoute'], ['location' => $location->getId()])
      ];
    }

    $form['table'] = [
      '#type' => 'table',
      '#header' => ['', t('Type'), t('Value'), t('Operation'), t('Access'), t('Apply to Descendants'), '', t('Weight')],
      '#tabledrag' => array(
        array(
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-order-weight',
        ),
      ),
    ];

    // Create each draggable row
    foreach($accessList as $rule){
      $form['table'][$rule['id']]['#attributes']['class'][] = 'draggable';
      $form['table'][$rule['id']]['#weight'] = $rule['weight'];
      $form['table'][$rule['id']]['empty_col'] = [
        '#plain_text' => ''
      ];
      $form['table'][$rule['id']]['type'] = [
        '#plain_text' => $permissionsManager->getEntitreePermissionsTypePlugin($rule['type'])->getPluginDefinition()['label']
      ];
      $form['table'][$rule['id']]['value'] = [
        '#plain_text' => $permissionsManager->getEntitreePermissionsTypePlugin($rule['type'])->formatAccessValue($rule['value']),
      ];
      $form['table'][$rule['id']]['operation'] = [
        '#plain_text' => $rule['operation'],
      ];
      $form['table'][$rule['id']]['access_type'] = [
        '#plain_text' => $rule['access_type'],
      ];
      $form['table'][$rule['id']]['apply_to_descendants'] = [
        '#plain_text' => $rule['apply_to_descendants'] == 1 ? '✔' : '',
      ];

      // Operations (dropbutton) column.
      $form['table'][$rule['id']]['operations'] = [
        '#type' => 'operations',
        '#links' => []
      ];
      $form['table'][$rule['id']]['operations']['#links']['edit'] = [
        'title' => t('Edit'),
        'url' => Url::fromRoute('entitree_permissions.access_edit', ['location' => $rule['location'], 'accessId' => $rule['id']])
      ];
      $form['table'][$rule['id']]['operations']['#links']['delete'] = [
        'title' => t('Delete'),
        'url' => Url::fromRoute('entitree_permissions.access_delete', ['location' => $rule['location'], 'accessRuleId' => $rule['id']])
      ];

      $form['table'][$rule['id']]['weight'] = [
        '#type' => 'weight',
        '#title' => t('Weight for rule'),
        '#title_display' => 'invisible',
        '#default_value' => $rule['weight'],
        '#attributes' => ['class' => ['table-order-weight']],
        '#delta' => 100
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Save'
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    // Update weights for access rules
    $values = $form_state->getValues();

    /**
     * @todo this could potentially fire off many DB calls if too many rules
     * were setup here. Is there a better approach?
     */
    $connection = \Drupal::service('database');
    foreach($values['table'] as $permissionId=>$row){
      // Check if the value has changed
      if($row['weight'] !== $form['table'][$permissionId]['weight']['#default_value']){
        $connection->update('entitree_permissions')
          ->fields(['weight' => $row['weight']])
          ->condition('id', $permissionId, '=')
          ->execute();
      }
    }

    // Rebuild the route cache to enforce new permissions
    // TODO see if this can be targeted to only impact the specific subtree.
    \Drupal::service('router.builder')->rebuild();

    $messenger = \Drupal::messenger();
    $messenger->addMessage('Your access rules have been saved', $messenger::TYPE_STATUS);
  }
}
