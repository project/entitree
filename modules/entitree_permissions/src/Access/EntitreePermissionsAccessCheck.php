<?php

namespace Drupal\entitree_permissions\Access;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\HttpFoundation\ParameterBag;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Entity\EntityAccessCheck;
use Drupal\Core\Entity\EntityInterface;

class EntitreePermissionsAccessCheck extends EntityAccessCheck{

  /**
   * Entitree access check to determine if current entity is managed by
   * Entitree, and if so, if the current user has access
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   */
  public function access(
    Route $route,
    RouteMatchInterface $route_match,
    AccountInterface $account
  ) {
    // AccountInterface::id returns a string so don't require a type match
    if($account->id() == 1){
      return AccessResult::allowed();
    }

    // Determine if this entity type is managed by Entitree
    $entitiesRequiringAcccessCheck = $this->filterParametersList(
      $route_match->getParameters());

    /**
     * If none of the entities require an access check fallback to the
     * Drupal core access check
     */
    if(count($entitiesRequiringAcccessCheck) === 0){
      return parent::access($route, $route_match, $account);
    }

    // Determine operation being requested
    $operation = $this->getRequestedOperation($route);

    // Load the access definitions for this location
    $activeAccessRule = $this->getActiveAccessRule(
      $entitiesRequiringAcccessCheck, $operation, $account);

    if($activeAccessRule){
      $entitreePermissionsManager =
        \Drupal::service('entitree_permissions.manager');
      return $entitreePermissionsManager
        ->getEntitreePermissionsTypePlugin($activeAccessRule['type'])
        ->checkUserAccess($account, $activeAccessRule);
    }

    /**
     * There are no applicable access rules. Fallback to the Drupal core
     * access check
     */
    return parent::access($route, $route_match, $account);
  }

  protected function filterParametersList(ParameterBag $parameters): array{
    // Get entity types managed by Entitree
    $entitreeManager = \Drupal::service("entitree.manager");
    $managedTypes = $entitreeManager->getConfig()->get('types');

    // Create array of all entities that need to be checked
    $entitiesRequiringAcccessCheck = [];

    // Check all entities in the path to see if any are managed by Entitree
    foreach($parameters as $parameter){
      if(
        is_object($parameter)
        && method_exists($parameter, "getEntityTypeId")
        && in_array($parameter->getEntityTypeId(), $managedTypes)
      ) {
        $entitiesRequiringAcccessCheck[] = $parameter;
      }
    }

    return $entitiesRequiringAcccessCheck;
  }

  protected function getRequestedOperation(Route $route): string{
    // Split the entity type and the operation.
    $requirement = $route->getRequirement('_entity_access');
    list($entity_type, $operation) = explode('.', $requirement);

    /**
     * @todo nodes receive the "update" operation instead of "edit", but the
     * definitions appear to use "edit." Converting "update" to "edit" for now
     * but this needs to be revisited.
     */
    $operation = $operation === 'update' ? 'edit' : $operation;

    return $operation;
  }

  protected function getActiveAccessRule(
    array $entitiesRequiringAcccessCheck,
    string $operation, AccountInterface $account
  ): ?array{
    $locationAccessDefinitions = [];

    foreach($entitiesRequiringAcccessCheck as $entity){
      // Create array for entity type if necessary
      $locationAccessDefinitions[$entity->getEntityTypeId()] =
        $locationAccessDefinitions[$entity->getEntityTypeId()] ?? [];

      // Create array for entity id
      $locationAccessDefinitions[$entity->getEntityTypeId()][$entity->id()] =
        [];

      /**
       * Check to see if the requested operation is only permissible on the main
       * location
       */
      $entitreeManager = \Drupal::service('entitree.manager');
      $entityTypeOperations = $entitreeManager->getEntityTypeOperations();

      if(
        isset($entityTypeOperations[$entity->getEntityTypeId()])
        && isset($entityTypeOperations[$entity->getEntityTypeId()][$operation])
        && isset($entityTypeOperations[$entity->getEntityTypeId()][$operation]['main_location_permission_only'])
        && $entityTypeOperations[$entity->getEntityTypeId()][$operation]['main_location_permission_only']
      ){
        /**
         * If this operation is only for the main location and one was found,
         * consider it the matching location
         */
        $matchingLocation = \Drupal::service('entitree.manager')
          ->getMainLocation($entity);
      }
      else{
        // Load the locations associated with this entity
        $allEntityLocations = \Drupal::service('entitree.manager')
          ->getLocationsByIdAndType($entity->id(), $entity->getEntityTypeId());

        // Load the current path alias
        /**
         * @todo This logic only works for view operations when on an alias for
         * the entity. This should be provided via context to this method to use
         * with other operations. This method will also return URL parameters
         * currently.
         */
        $alias = \Drupal::request()->getPathInfo();

        /**
         * This check may be done when on an alias for this node, but it may also
         * be done at other times.
         * If this alias is for the current entity then use it to apply access
         * rules.
         */

        // Check if any locations match the current path
        $matchingLocation = null;
        foreach ($allEntityLocations as $location) {
          if ($location->getPath() === $alias) {
            $matchingLocation = $location;
            break;
          }
        }
      }

      // If no location exists for the specified path move to the next entity
      if(!$matchingLocation){
        continue;
      }

      $entitreePermissionsManager =
        \Drupal::service('entitree_permissions.manager');

      // Load access definitions for the matching location
      $locationAccessList = $entitreePermissionsManager
        ->getAccessList($matchingLocation, null, $operation);

      /**
       * If a rule is defined for this operation on this location, see if it is
       * applicable to this user
       */
      if(count($locationAccessList) > 0){
        /**
         * By ascending access rule weight, check if any rules apply to this
         * user
         */
        foreach($locationAccessList as $locationAccessRule){
          /**
           * @todo Instead of calling each rule access check once, this should
           * be refactored so that the list can be filtered previously to remove
           *  rules that aren't applicable with fewer steps
           */
          $accessResult = $entitreePermissionsManager
            ->getEntitreePermissionsTypePlugin(
              $locationAccessRule['type'])->checkUserAccess($account,
                $locationAccessRule);

          if(!$accessResult->isNeutral()){
            // Found the applicable access rule for this request
            return $locationAccessRule;
          }
        }
      }


      /**
       * No rule found for this location and operation. Check ancestors for
       * applicable rule
       */
      // Load relevant access definitions for ancestor locations
      $ancestorsAccessList = $entitreePermissionsManager
        ->getAncestorsAccessList($matchingLocation, $operation);

      /**
       * The ancestors list is returned with the site root first. Reverse the
       * order so we step through in the opposite direction
       */
      $ancestorsAccessList = array_reverse($ancestorsAccessList);

      /**
       * Step through each ancestor starting with the parent. Check each access
       * rule and return the first one applicable to this user
       */
      foreach($ancestorsAccessList as $ancestorAccessList){
        foreach($ancestorAccessList as $accessRule){
          /**
           * @todo Instead of calling each rule access check once, this should
           * be refactored so that the list can be filtered previously to remove
           *  rules that aren't applicable with fewer steps
           */
          $accessResult = $entitreePermissionsManager
            ->getEntitreePermissionsTypePlugin($accessRule['type'])
            ->checkUserAccess($account, $accessRule);

          if (!$accessResult->isNeutral()) {
            // Found the applicable access rule for this request
            return $accessRule;
          }
        }
      }
    }

    return null;
  }
}
