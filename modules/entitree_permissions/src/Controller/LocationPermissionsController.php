<?php

namespace Drupal\entitree_permissions\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\entitree\EntitreeManagerInterface;
use Drupal\entitree\EntitreeLocationInterface;
use Drupal\entitree_permissions\EntitreePermissionsManagerInterface;
use Drupal\entitree_permissions\Form\EntitreePermissionsLocationAccessRulesForm;

/**
 * Class LocationPermissionsController.
 */
class LocationPermissionsController extends ControllerBase {

  /**
   * Drupal\entitree\EntitreeManagerInterface definition.
   *
   * @var \Drupal\entitree\EntitreeManagerInterface
   */
  protected $entitreeManager;

  /**
   * @var EntitreePermissionsManagerInterface
   */
  protected $entitreePermissionsManager;

  /**
   * Constructs a new LocationPermissionsController object.
   */
  public function __construct(EntitreeManagerInterface $entitree_manager, EntitreePermissionsManagerInterface $entitree_permissions_manager) {
    $this->entitreeManager = $entitree_manager;
    $this->entitreePermissionsManager = $entitree_permissions_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entitree.manager'),
      $container->get('entitree_permissions.manager')
    );
  }

  /**
   * Render.
   *
   */
  public function render(EntitreeLocationInterface $location): array{
    // Get enabled types from the entitree entity type manager
    $permissionsTypeManager = \Drupal::service('plugin.manager.entitree_permissions_type');
    $types = $permissionsTypeManager->getDefinitions();

    foreach($types as &$type){
      // Get instance of permissions type Plugin
      $permissionTypePlugin = $this->entitreePermissionsManager->getEntitreePermissionsTypePlugin($type['id']);

      $type['addRoute'] = $permissionTypePlugin->getAddRoute();
    }

    $table = \Drupal::formBuilder()->getForm(EntitreePermissionsLocationAccessRulesForm::class, $location, $this->entitreePermissionsManager->getAccessList($location), $types);

    return [
      '#theme' => 'entitree_permissions__location',
      '#location' => $location,
      '#table' => $table
    ];
  }

  public function accessEditDisplay(EntitreeLocationInterface $location, int $accessId){
    // Load access record
    $connection = \Drupal::database();
    $query = $connection->query("SELECT * from {entitree_permissions} WHERE id = :id LIMIT 0,1", [":id" => $accessId], ['fetch' => \PDO::FETCH_ASSOC]);
    $result = $query->fetchAll()[0];

    // Render form for appropriate permission type
    $plugin = \Drupal::service('plugin.manager.entitree_permissions_type')->createInstance($result['type']);
    $form = $plugin->buildEditForm($result);

    return $form;
  }
}
