<?php

namespace Drupal\entitree_permissions\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Entitree permissions type plugin manager.
 */
class EntitreePermissionsTypeManager extends DefaultPluginManager {


  /**
   * Constructs a new EntitreePermissionsTypeManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/EntitreePermissionsType', $namespaces, $module_handler, 'Drupal\entitree_permissions\Plugin\EntitreePermissionsTypeInterface', 'Drupal\entitree_permissions\Annotation\EntitreePermissionsType');

    $this->alterInfo('entitree_permissions_entitree_permissions_type_info');
    $this->setCacheBackend($cache_backend, 'entitree_permissions_entitree_permissions_type_plugins');
  }

}
