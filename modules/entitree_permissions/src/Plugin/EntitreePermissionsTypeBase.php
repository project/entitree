<?php

namespace Drupal\entitree_permissions\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\entitree\EntitreeLocationInterface;

/**
 * Base class for Entitree permissions type plugins.
 */
abstract class EntitreePermissionsTypeBase extends PluginBase implements EntitreePermissionsTypeInterface {

}
