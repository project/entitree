<?php

namespace Drupal\entitree_permissions\Plugin\EntitreePermissionsType;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\entitree_permissions\Plugin\EntitreePermissionsTypeBase;
use Drupal\entitree\EntitreeLocationInterface;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides entitree permissions support for roles
 *
 * @EntitreePermissionsType(
 *   id = "role",
 *   label = "Role"
 * )
 */
class EntitreePermissionsTypeRole extends EntitreePermissionsTypeBase {
  public function getAddRoute(): string{
    return "entitree_permissions.role_access";
  }

  public function formatAccessList(array $accessList): array{
    $formattedList = [];

    $roles = user_roles();

    foreach($accessList as $access){
        $access['value'] = $roles[$access['value']]->label();
        $formattedList[] = $access;
    }

    return $formattedList;
  }

  /**
   * @inheritDoc
   */
  public function formatAccessValue($value): string
  {
    $roles = user_roles();

    return $roles[$value]->label();
  }

  /**
   * @inheritDoc
   */
  public function checkUserAccess(AccountInterface $account, array $accessRule): AccessResultInterface{
    // Determine if the provided access rule is applicable for this user
    $userRoles = $account->getRoles();

    if(in_array($accessRule['value'], $userRoles)){
      // If it is applicable, determine if access is allowed or denied and return the appropriate response
      switch ($accessRule['access_type']) {
        case "allowed":
          return AccessResult::allowed();
        case "denied":
          return AccessResult::forbidden();
      }
    }

    // If not applicable return neutral
    return AccessResult::neutral();
  }

  public function buildEditForm(array $permission): array
  {
    return \Drupal::formBuilder()->getForm('\Drupal\entitree_permissions\Form\RoleAccessForm', $permission);
  }
}
