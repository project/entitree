<?php

namespace Drupal\entitree_permissions\Plugin\EntitreePermissionsType;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\entitree_permissions\Plugin\EntitreePermissionsTypeBase;
use Drupal\entitree\EntitreeLocationInterface;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides entitree permissions support for users
 *
 * @EntitreePermissionsType(
 *   id = "user",
 *   label = "User"
 * )
 */
class EntitreePermissionsTypeUser extends EntitreePermissionsTypeBase
{
  public function getAddRoute(): string
  {
    return "entitree_permissions.user_access";
  }

  public function formatAccessList(array $accessList): array
  {
    return $accessList;
  }

  public function formatAccessValue($userId): string{
    $user = \Drupal\user\Entity\User::load($userId);
    return $user->getUsername();
  }

  public function checkUserAccess(AccountInterface $account, array $accessRule): AccessResultInterface
  {
    // Determine if the provided access rule is applicable for this user
    if($account->id() === $accessRule['value']){
      // If it is applicable, determine if access is allowed or denied and return the appropriate response
      switch ($accessRule['access_type']) {
        case "allowed":
          return AccessResult::allowed();
        case "denied":
          return AccessResult::forbidden();
      }
    }

    // If not applicable return neutral
    return AccessResult::neutral();
  }

  public function buildEditForm(array $permission): array
  {
    return \Drupal::formBuilder()->getForm('\Drupal\entitree_permissions\Form\UserAccessForm', $permission);
  }
}
