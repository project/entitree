<?php

namespace Drupal\entitree_permissions\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\entitree\EntitreeLocationInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Form\FormInterface;

/**
 * Defines an interface for Entitree permissions type plugins.
 */
interface EntitreePermissionsTypeInterface extends PluginInspectionInterface {
  public function getAddRoute(): string;
  public function formatAccessList(array $accessList): array;

  /**
   * Receives an access rule value and formats it to a user friendly string
   *
   * @param int|string $value
   * @return string
   */
  public function formatAccessValue($value): string;

  /**
   * Check an access list against a user account to determine if the user has
   * any matching rules and if so do they pass
   *
   * @param AccountInterface $account
   * @param array $accessList
   * @return bool
   */
  public function checkUserAccess(AccountInterface $account, array $accessList): AccessResultInterface;

  public function buildEditForm(array $permission): array;
}
