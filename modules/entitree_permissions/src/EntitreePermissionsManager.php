<?php

namespace Drupal\entitree_permissions;

use Drupal\mysql\Driver\Database\mysql\Connection;
use Drupal\entitree\EntitreeLocationInterface;
use Drupal\entitree_permissions\Plugin\EntitreePermissionsTypeInterface;
use Drupal\entitree_permissions\Plugin\EntitreePermissionsTypeManager;

/**
 * Class EntitreePermissionsManager.
 */
class EntitreePermissionsManager implements EntitreePermissionsManagerInterface
{

  /**
   * Drupal\mysql\Driver\Database\mysql\Connection definition.
   *
   * @var \Drupal\mysql\Driver\Database\mysql\Connection
   */
  protected $database;

  protected $entitreePermissionsTypeManager;

  protected $permissionTypePlugins = [];

  protected $permissionTypeDefinitions;

  /**
   * Constructs a new EntitreePermissionsManager object.
   */
  public function __construct(
    Connection $database,
    EntitreePermissionsTypeManager $entitreePermissionsTypeManager
  ) {
    $this->database = $database;
    $this->entitreePermissionsTypeManager = $entitreePermissionsTypeManager;
  }

  public function loadAccessRule(int $id): ?array
  {
    $query = "SELECT * FROM {entitree_permissions} WHERE id = :id";
    $query = $this->database->query($query, [":id", $id]);
    return $query->fetchAssoc();
  }

  public function getAccessList(
    EntitreeLocationInterface $location,
    string $type = null,
    string $operation = null
  ) {
    $query = "SELECT * from {entitree_permissions} WHERE location = :location ";
    $parameters = [
      ":location" => $location->getId()
    ];

    if ($type) {
      $query .= " AND `type` = :type ";
      $parameters[":type"] = $type;
    }

    if ($operation) {
      $query .= " AND operation = :operation ";
      $parameters[":operation"] = $operation;
    }

    $query .= " ORDER BY weight";

    // Load all records from database for this location
    $query = $this->database->query(
      $query,
      $parameters,
      ['fetch' => \PDO::FETCH_ASSOC]
    );
    $result = $query->fetchAll();

    return $result;
  }

  public function getAncestorsAccessList(
    EntitreeLocationInterface $location,
    string $operation = null
  ) {
    // Get list of ancestor IDs
    $ancestors = \Drupal::service('entitree.manager')
      ->getStore($location->getLangcode())
      ->getLocationAncestors($location->getId());
    $ancestorIDs = array_map(function ($ancestor) {
      return $ancestor->getId();
    }, $ancestors);

    if ($operation) {
      $query = $this->database->query(
        "SELECT * from {entitree_permissions}
          WHERE location IN (:ancestors[])
          AND apply_to_descendants = 1
          AND operation = :operation",
        [
          ":ancestors[]" => $ancestorIDs,
          ":operation" => $operation
        ],
        ['fetch' => \PDO::FETCH_ASSOC]
      );
    } else {
      $query = $this->database->query(
        "SELECT * from {entitree_permissions}
          WHERE location IN (:ancestors[])
          AND apply_to_descendants = 1",
        [":ancestors[]" => $ancestorIDs],
        ['fetch' => \PDO::FETCH_ASSOC]
      );
    }
    $result = $query->fetchAll();

    // Create array of rules grouped by ancestor
    $groupedRules = [];
    foreach ($result as $row) {
      $groupedRules[$row['location']] = $groupedRules[$row['location']] ?? [];

      $groupedRules[$row['location']][] = $row;
    }

    /**
     * Create an ordered array so that the site root is first and the parent is
     * last
     */
    $orderedArray = [];
    foreach ($ancestorIDs as $id) {
      // Skip locations that don't have any rules.
      if(isset($groupedRules[$id])){
        $orderedArray[$id] = $groupedRules[$id];
      }
    }

    return $orderedArray;
  }

  public function getEntitreePermissionsTypePlugin(
    string $id
  ): EntitreePermissionsTypeInterface {
    if (!isset($this->permissionTypePlugins[$id])) {
      // Plugins not yet loaded

      // Load type definitions if needed
      if (!isset($this->permissionTypeDefinitions)) {
        $this->permissionTypeDefinitions = $this
          ->entitreePermissionsTypeManager
          ->getDefinitions();
      }

      foreach ($this->permissionTypeDefinitions as $type) {
        if ($type['id'] === $id) {
          $this->permissionTypePlugins[$id] = $this
            ->entitreePermissionsTypeManager
            ->createInstance($type['id']);
          break;
        }
      }
    }

    return $this->permissionTypePlugins[$id];
  }
}
