<?php

namespace Drupal\entitree_permissions\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Event subscriber to update the operations array for Entitree Permissions.
 */
class EntitreePermissionsEventsSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a new EntitreePermissionsEventsSubscriber object.
   */
  public function __construct() {

  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['entitree_operations'] = ['entitreeOperations'];

    return $events;
  }

  /**
   * This method is called when the entitree_operations is dispatched.
   *
   * @param \Symfony\Contracts\EventDispatcher\Event $event
   *   The dispatched event.
   */
  public function entitreeOperations(Event $event) {
    /** @var \Drupal\entitree\Event\EntitreeOperationsEvent $event */
    $operations = $event->getOperations();

    /*
     * Flag edit and delete operations as only have defined permissions on the
     * main location
     */
    foreach ($operations as $operation => $values) {
      if (in_array($operation, ['edit', 'delete'])) {
        $operations[$operation]['main_location_permission_only'] = TRUE;
      }
    }

    $event->setOperations($operations);
  }

}
