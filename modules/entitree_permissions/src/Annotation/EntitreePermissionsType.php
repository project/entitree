<?php

namespace Drupal\entitree_permissions\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Entitree permissions type item annotation object.
 *
 * @see \Drupal\entitree_permissions\Plugin\EntitreePermissionsTypeManager
 * @see plugin_api
 *
 * @Annotation
 */
class EntitreePermissionsType extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
