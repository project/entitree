<?php

namespace Drupal\entitree_node\Plugin\Menu\LocalAction;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Modifies the 'Add link' local action to add a destination.
 */
class EntitreeNodeLocationAddAction extends LocalActionDefault {

  public function getRouteParameters(RouteMatchInterface $route_match) {
    $parameters = parent::getRouteParameters($route_match);

    // Add the route parameter for entity ID.
    $parameters['entityId'] = \Drupal::request()->attributes->get('node');

    return $parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptions(RouteMatchInterface $route_match) {
    $options = parent::getOptions($route_match);

    // Append the current path as destination to the query string.
    $options['query']['destination'] = \Drupal::service('path.current')
      ->getPath();
    return $options;
  }

}
