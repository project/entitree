<?php

namespace Drupal\entitree_node\Plugin\Menu\LocalAction;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Modifies the 'Organize Locations' local action to add the node.
 */
class EntitreeNodeOrganizeLocationsAction extends LocalActionDefault {

  public function getRouteParameters(RouteMatchInterface $route_match) {
    $parameters = parent::getRouteParameters($route_match);

    // Add the route parameter for entity ID.
    $parameters['id'] = \Drupal::request()->attributes->get('node');

    return $parameters;
  }

}
