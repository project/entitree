<?php

namespace Drupal\entitree_node\Plugin\EntitreeEntityType;

use Drupal\entitree\Plugin\EntitreeEntityTypeBase;
use Symfony\Component\Routing\Route;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;

/**
 * Provides entitree support for nodes.
 *
 * @EntitreeEntityType(
 *   id = "node",
 *   entity_id = "node",
 *   entity_name = "Node"
 * )
 */
class EntitreeNode extends EntitreeEntityTypeBase {

  /**
   * {@inheritDoc}
   */
  public function buildManageLocationRoute(array &$routes) {
    /**
     * @todo this throws an error with the devel module because it seems to have
     * a route that requires the first parameter here to be named {node}. We
     * need it to be named something generic as this controller is used for all
     * entity types. This seems like a problem with the devel module but we may
     * need to find a workaround
     */
    $route = (new Route('/node/{node}/locations/{entity_type}'))
      ->addDefaults([
        '_controller' => '\Drupal\entitree\Controller\EntitreeDisplayController::renderEntityLocations',
        '_title' => 'Manage Locations',
        'entity_type' => 'node'
      ])
      ->setRequirement('node', '\d+')
      /**
       * @todo setup permissions using logic that checks the manage locations
       * permissions
       */
      ->setRequirement('_permission', 'administer entitree')
      ->setOption('_node_operation_route', TRUE);
    $routes['entitree.locations.node'] = $route;

    $route = (new Route('/node/{id}/locations/organize/{entity_type}'))
      ->addDefaults([
        '_form' => '\Drupal\entitree\Form\LocationOrganizeForm',
        '_title' => 'Confirm Location Organization',
        'entity_type' => 'node',
      ])
      ->setRequirement('id', '^\d+$')
      /**
       * @todo setup permissions using logic that checks the manage locations
       * permissions
       */
      ->setRequirement('_permission', 'administer entitree')
      ->setOption('_node_operation_route', TRUE);
    $routes['entitree.locations.organize'] = $route;
  }

  /**
   * Apply token replace action to a string using values from the provided entity.
   *
   * @param string $value
   * @param Node $entity
   * @return string
   */
  public static function applyTokenReplaceActions(string $value, $entity): string {
    return \Drupal::service('token')->replace($value, [
      'node' => $entity
    ]);
  }

  /**
   * {@inheritDoc}
   */
  public function getSourcePath($id): string {
    return "/node/" . $id;
  }

  public function loadEntityByRouteParams(array $params): EntityInterface {
    return Node::load($params['node']);
  }

  public function loadEntityById(int $id): EntityInterface {
    return Node::load($id);
  }

  /**
   * Generate a URL object to link to an entity's manage locations route.
   *
   * @param int $id
   *   Entity ID.
   *
   * @return \Drupal\Core\Url
   *   URL object directing to the entity's manage locations route.
   */
  public function getLocationsPathById(int $id): Url {
    return Url::fromRoute('entitree.locations.node', [
      "node" => $id,
    ]);
  }

  public function getLocalTasks($base_plugin_definition): array {
    $tasks = [];

    $tasks['entitree.entity_edit_display'] = $base_plugin_definition;
    $tasks['entitree.entity_edit_display']['title'] = "Manage Locations";
    $tasks['entitree.entity_edit_display']['route_name'] = 'entitree.locations.node';
    $tasks['entitree.entity_edit_display']['base_route'] = "entity.node.canonical";

    return $tasks;
  }

  /**
   * {@inheritDoc}
   */
  public function getOperations(): array {
    $operations = parent::getOperations();

    return $operations;
  }

  /**
   * {@inheritDoc}
   */
  public static function getTokenTypes(): array {
    return ['node'];
  }

  /**
   * {@inheritDoc}
   */
  public static function getExistingPathAliases(int $id): array {
    /** @var \Drupal\Core\Entity\EntityStorageBaseInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('path_alias');
    $result = $storage->loadByProperties(['path' => "/node/$id"]);

    return $result;
  }

  /**
   * {@inheritDoc}
   */
  public static function getOrphanedpathAliases(int $id): array {
    /** @var \Drupal\path_alias\PathAliasInterface[] $pathAliases */
    $pathAliases = self::getExistingPathAliases($id);

    // Determine which of these have locations for this node.
    /** @var \Drupal\entitree\EntitreeManagerInterface $entitreeManager */
    $entitreeManager = \Drupal::service('entitree.manager');
    $locations = $entitreeManager->getLocationsByIdAndType($id, 'node');

    $orphaned = [];
    foreach ($pathAliases as $alias) {
      // Search locations for one associated with this path alias ID.
      foreach ($locations as $location) {
        if ($location->get('pid')->getString() == $alias->id()) {
          continue 2;
        }
      }

      // If reaching this point the alias doesn't have a corresponding location.
      $orphaned[] = $alias;
    }

    return $orphaned;
  }

}
