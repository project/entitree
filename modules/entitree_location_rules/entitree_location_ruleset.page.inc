<?php

/**
 * @file
 * Contains entitree_location_ruleset.page.inc.
 *
 * Page callback for Entitree location ruleset entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Entitree location ruleset templates.
 *
 * Default template: entitree_location_ruleset.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_entitree_location_ruleset(array &$variables) {
  // Fetch EntitreeLocationRuleset Entity Object.
  $entitree_location_ruleset = $variables['elements']['#entitree_location_ruleset'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
