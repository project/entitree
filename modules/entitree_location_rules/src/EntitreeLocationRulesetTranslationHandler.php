<?php

namespace Drupal\entitree_location_rules;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for entitree_location_ruleset.
 */
class EntitreeLocationRulesetTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
