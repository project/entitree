<?php

namespace Drupal\entitree_location_rules\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Entitree location rule item annotation object.
 *
 * @see \Drupal\entitree_location_rules\Plugin\EntitreeLocationRuleManager
 * @see plugin_api
 *
 * @Annotation
 */
class EntitreeLocationRule extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * Flag if the rule type can be used multiple times in a single ruleset.
   *
   * @var bool
   */
  public $unique;

}
