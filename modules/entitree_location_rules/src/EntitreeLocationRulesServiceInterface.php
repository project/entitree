<?php

namespace Drupal\entitree_location_rules;

use Drupal\Core\Entity\EntityInterface;

/**
 * Service to provide logic for Entitree Location Rules.
 */
interface EntitreeLocationRulesServiceInterface {

  /**
   * Apply relevant rulesets to the provided entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to generate locations.
   */
  public function applyLocationRules(EntityInterface $entity);

  /**
   * Given a string convert it to a URL-friendly format.
   *
   * @param string $path
   *   Path to clean.
   *
   * @return string
   *   Cleaned path.
   */
  public function cleanPathString(string $path): string;

}
