<?php

namespace Drupal\entitree_location_rules;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entitree\EntitreeManagerInterface;
use Drupal\entitree\Entity\EntitreeLocation;
use Drupal\entitree_location_rules\Entity\EntitreeLocationRuleset;
use Drupal\entitree_location_rules\Entity\EntitreeLocationRulesetInterface;

/**
 * A service class to provider logic for Entitree Location Rules.
 */
class EntitreeLocationRulesService implements EntitreeLocationRulesServiceInterface {

  /**
   * Drupal\entitree\EntitreeManagerInterface definition.
   *
   * @var \Drupal\entitree\EntitreeManagerInterface
   */
  protected $entitreeManager;

  /**
   * Constructs a new EntitreeLocationRulesService object.
   */
  public function __construct(EntitreeManagerInterface $entitree_manager) {
    $this->entitreeManager = $entitree_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function applyLocationRules(EntityInterface $entity) {
    // Verify entity type is enabled in Entitree config.
    $enabledEntityTypeIds = $this->entitreeManager->getConfig()->get('types');
    if (!in_array($entity->getEntityTypeId(), $enabledEntityTypeIds)) {
      return;
    }

    // Load all rules.
    $rulesets = EntitreeLocationRuleset::loadMultiple();

    $plugins = [];

    foreach ($rulesets as $ruleset) {
      $config = $ruleset->getRules();

      foreach ($config as $ruleConfig) {
        if (!isset($plugins[$ruleConfig['rule_type']])) {
          $plugins[$ruleConfig['rule_type']] = \Drupal::service('plugin.manager.entitree_location_rule')->getEntitreeEntityType($ruleConfig['rule_type']);
        }

        if (!$plugins[$ruleConfig['rule_type']]->checkEntityAgainstConfig($ruleConfig['value'], $entity)) {
          // If the entity fails any rules in ruleset do not create a location.
          continue 2;
        }
      }

      // All rules have passed. Create the locations.
      $this->createRulesetLocations($ruleset, $entity);
    }
  }

  /**
   * Create locations based on the destinations property for a given entity.
   *
   * @param \Drupal\entitree_location_rules\Entity\EntitreeLocationRulesetInterface $ruleset
   *   The ruleset containing the destinations.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity receiving the locations.
   */
  protected function createRulesetLocations(EntitreeLocationRulesetInterface $ruleset, EntityInterface $entity) {
    /*
     * For each destination defined for this ruleset determine if the entity
     * already has a location. If not, create one.
     */
    $destinations = $ruleset->getDestinations();

    /** @var \Drupal\entitree\EntitreeLocationInterface[] $locations */
    $locations = \Drupal::service('entitree.manager')->getLocationsByIdAndType($entity->id(), $entity->getEntityTypeId());

    foreach ($destinations as $destination) {
      // Generate the label and path segment.
      $label = $this->entitreeManager->applyEntityTokenReplaceAction($ruleset->getLocationLabel(), $entity);
      $pathSegment = $this->entitreeManager->applyEntityTokenReplaceAction($ruleset->getLocationPathSegment(), $entity);

      /*
       * $destination is the parent location entity. Check the provided entity's
       * locations to see if the parent matches.
       */
      foreach ($locations as $location) {
        if ($location->getParent()->getId() === $destination->getId()) {
          /*
           * If the location already exists under this parent then check to see
           * if the path segment has changed. If so, update the location with
           * the new path segment.
           */
          if ($location->getPathSegment() !== $pathSegment) {
            $location->set('path_segment', $this->cleanPathString($pathSegment));
            $location->save();
            $location->applyEntitreeUpdates(TRUE);
          }
          continue 2;
        }
      }

      /*
       * If reaching this point then no location exists as a child of this
       * destination.
       */

      $newLocation = EntitreeLocation::create([
        "label" => $label,
        /*
         * TODO replace with a Drupal function to generate machine name if
         * possible.
         * TODO add logic to look for duplicate path segment and increment if
         * needed.
         */
        "path_segment" => $this->cleanPathString($pathSegment),
        "ref_entity_type" => $entity->getEntityTypeId(),
        "ref_entity_id" => $entity->id(),
        // TODO get langcode from entity.
        "langcode" => \Drupal::languageManager()->getCurrentLanguage()->getId(),
      ]);

      $newLocation->setParent($destination);
      $newLocation->save();
      $newLocation->applyEntitreeUpdates(FALSE, $destination);
    }
  }

  /**
   * Given a string convert it to a URL-friendly format.
   *
   * @param string $path
   *   The path to clean.
   *
   * @return string
   *   The cleaned path.
   */
  public function cleanPathString(string $path): string {
    // Ensure that escaped single and double quotes are always removed.
    $pathWithReplacements = preg_replace('@&quot;|&#039;@', '', strtolower($path));
    // Remove all non-numeric characters.
    $pathWithReplacements = preg_replace('@[^a-z0-9-]+@', '-', $pathWithReplacements);
    // Return string with preceeding and trailing hyphens removed.
    return preg_replace('@^-+|-+$@', '', $pathWithReplacements);
  }

}
