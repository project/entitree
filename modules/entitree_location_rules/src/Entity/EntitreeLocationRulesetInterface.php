<?php

namespace Drupal\entitree_location_rules\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface for defining Entitree location ruleset entities.
 *
 * @ingroup entitree_location_rules
 */
interface EntitreeLocationRulesetInterface extends ContentEntityInterface, EntityChangedInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Entitree location ruleset name.
   *
   * @return string
   *   Name of the Entitree location ruleset.
   */
  public function getLabel() :string;

  /**
   * Sets the Entitree location ruleset name.
   *
   * @param string $label
   *   The Entitree location ruleset name.
   *
   * @return \Drupal\entitree_location_rules\Entity\EntitreeLocationRulesetInterface
   *   The called Entitree location ruleset entity.
   */
  public function setLabel(string $label);

  /**
   * Gets the Entitree location ruleset creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Entitree location ruleset.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the Entitree location ruleset creation timestamp.
   *
   * @param int $timestamp
   *   The Entitree location ruleset creation timestamp.
   *
   * @return \Drupal\entitree_location_rules\Entity\EntitreeLocationRulesetInterface
   *   The called Entitree location ruleset entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Get the ruleset rules configuration.
   *
   * @return array
   *   Array of configuration values for the defined rules.
   */
  public function getRules(): array;

  /**
   * Set the ruleset entity rules configuration.
   *
   * @param array $rules
   *   Array of rules config.
   */
  public function setRules(array $rules);

  /**
   * Get the ruleset destinations.
   *
   * @return \Drupal\entitree\Entity\EntitreeLocation[]
   *   Array of EntitreeLocation entities.
   */
  public function getDestinations(): array;

  /**
   * Get the pattern for the location label.
   *
   * @return string
   *   Pattern used for the label when creating locations with this ruleset.
   */
  public function getLocationLabel(): string;

  /**
   * Get the path segment pattern for this ruleset.
   *
   * @return string
   *   Pattern used for this path segment when creating locations.
   */
  public function getLocationPathSegment(): string;

}
