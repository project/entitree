<?php

namespace Drupal\entitree_location_rules\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\entitree\Entity\EntitreeLocation;

/**
 * Defines the Entitree location ruleset entity.
 *
 * @ingroup entitree_location_rules
 *
 * @ContentEntityType(
 *   id = "entitree_location_ruleset",
 *   label = @Translation("Entitree location ruleset"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\entitree_location_rules\EntitreeLocationRulesetListBuilder",
 *     "views_data" = "Drupal\entitree_location_rules\Entity\EntitreeLocationRulesetViewsData",
 *     "translation" = "Drupal\entitree_location_rules\EntitreeLocationRulesetTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\entitree_location_rules\Form\EntitreeLocationRulesetForm",
 *       "add" = "Drupal\entitree_location_rules\Form\EntitreeLocationRulesetForm",
 *       "edit" = "Drupal\entitree_location_rules\Form\EntitreeLocationRulesetForm",
 *       "delete" = "Drupal\entitree_location_rules\Form\EntitreeLocationRulesetDeleteForm",
 *     },
 *     "access" = "Drupal\entitree_location_rules\EntitreeLocationRulesetAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\entitree_location_rules\EntitreeLocationRulesetHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "entitree_location_ruleset",
 *   data_table = "entitree_location_ruleset_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer entitree",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/entitree/rules/{entitree_location_ruleset}",
 *     "add-form" = "/admin/structure/entitree/rules/add",
 *     "edit-form" = "/admin/structure/entitree/rules/{entitree_location_ruleset}/edit",
 *     "delete-form" = "/admin/structure/entitree/rules/{entitree_location_ruleset}/delete",
 *     "collection" = "/admin/structure/entitree/rules",
 *   },
 *   field_ui_base_route = "entitree_location_ruleset.settings"
 * )
 */
class EntitreeLocationRuleset extends ContentEntityBase implements EntitreeLocationRulesetInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function getLabel(): string {
    $labelValue = $this->get('label');
    $val = $labelValue->value;
    return $this->get('label')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLabel($label) {
    $this->set('label', $label);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getRules(): array {
    /** @var \Drupal\Core\Field\MapFieldItemList $rules */
    $rulesString = $this->get('rules_string')->getString();

    if ($rulesString) {
      return unserialize($rulesString);
    }

    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function setRules(array $rules) {
    $this->set('rules_string', serialize($rules));
  }

  /**
   * Array of entitree entity IDs representing the parents for new locations.
   *
   * @var array
   */
  protected $destinations;

  /**
   * {@inheritDoc}
   */
  public function getDestinations(): array {
    /** @var \Drupal\Core\Field\FieldItemListInterface $destinations */
    $destinations = $this->get('destinations');

    $destinationIds = array_map(function ($value) {
      return $value['value'];
    }, $destinations->getValue());

    if (count($destinationIds) > 0) {
      return EntitreeLocation::loadMultiple($destinationIds);
    }

    return [];
  }

  /**
   * {@inheritDoc}
   */
  public function getLocationLabel(): string {
    return $this->get('location_label')->getString();
  }

  /**
   * {@inheritDoc}
   */
  public function getLocationPathSegment(): string {
    return $this->get('location_path_segment')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the entitree location ruleset entity.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    // Unique beyond the scope of Entitree.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the entitree location ruleset entity.'))
      ->setRequired(TRUE)
      ->setReadOnly(TRUE);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setDescription(t('The label of the entitree location entity.'))
      ->setRequired(TRUE)
      ->setReadOnly(FALSE);

    /*
     * TODO this should proably be a map field type, but only the first value
     * was saving.
     */
    $fields['rules_string'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Rules'))
      ->setDescription(t('Configuration of rules for the entitree location ruleset entity.'))
      ->setRequired(TRUE)
      ->setDefaultValue(FALSE);

    $fields['location_label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Location Label'))
      ->setDescription(
      t('The location label of the entitree location ruleset entity.')
    )
      ->setRequired(TRUE)
      ->setReadOnly(FALSE);

    $fields['location_path_segment'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Location Path Segment'))
      ->setDescription(
      t('The location path segment of the entitree location ruleset entity.')
    )
      ->setRequired(TRUE)
      ->setReadOnly(FALSE);

    $fields['destinations'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('Destinations'))
      ->setDescription(
      t('The destinations of the entitree location ruleset entity.')
    )
      ->setRequired(FALSE)
      ->setReadOnly(FALSE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

}
