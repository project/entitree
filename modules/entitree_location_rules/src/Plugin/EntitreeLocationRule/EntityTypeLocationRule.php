<?php

namespace Drupal\entitree_location_rules\Plugin\EntitreeLocationRule;

use Drupal\entitree_location_rules\Plugin\EntitreeLocationRuleBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides rule based on entity type.
 *
 * @EntitreeLocationRule(
 *   id = "entity_type",
 *   label = "Entity Type",
 *   unique = true
 * )
 */
class EntityTypeLocationRule extends EntitreeLocationRuleBase {

  /**
   * {@inheritDoc}
   */
  public function generateFormElement(array $value, array $rules): array {
    $enabledEntityTypeIds = \Drupal::service('entitree.manager')
      ->getConfig()
      ->get('types');

    /*
     * Load human friendly labels for enabled types
     */
    $entityTypes = [];
    $definitions = \Drupal::entityTypeManager()->getDefinitions();
    foreach ($enabledEntityTypeIds as $id) {
      /*
       * Exclude the empty location type
       */
      if ($id === 'empty_location') {
        continue;
      }

      if (isset($definitions[$id])) {
        $entityTypes[$id] = $definitions[$id]->getLabel();
      }
    }

    return [
      '#type' => 'checkboxes',
      '#title' => t('Apply to any of these entity types'),
      '#options' => $entityTypes,
      '#default_value' => $value,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function checkEntityAgainstConfig(array $value, EntityInterface $entity): bool {
    return isset($value[$entity->getEntityTypeId()]) && $value[$entity->getEntityTypeId()];
  }

}
