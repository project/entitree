<?php

namespace Drupal\entitree_location_rules\Plugin\EntitreeLocationRule;

use Drupal\entitree_location_rules\Plugin\EntitreeLocationRuleBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides rule based on entity type.
 *
 * @EntitreeLocationRule(
 *   id = "entity_bundle",
 *   label = "Entity Bundle",
 *   unique = true,
 *   dependencies = {
 *     "entity_type"
 *   }
 * )
 */
class EntityBundleLocationRule extends EntitreeLocationRuleBase {

  /**
   * {@inheritDoc}
   */
  public function generateFormElement(array $value, array $rules): array {
    /*
     * Find the entity type rule config and use it to determine what entity
     * types have been selected in order to collect bundles.
     */
    $entityTypeConfig = NULL;
    foreach ($rules as $rule) {
      if ($rule['rule_type'] === 'entity_type') {
        $entityTypeConfig = $rule;
      }
    }

    if (!$entityTypeConfig) {
      throw new \Exception(\t('The entity bundle location rule cannot be used in
       a ruleset without an entity type location rule already defined.'));
    }

    // Collect bundles for each of the selected entity types.
    $types = [];
    $bundleInfoService = \Drupal::service('entity_type.bundle.info');
    foreach ($entityTypeConfig['value'] as $type) {
      if ($type === 0) {
        // This entity type is not enabled in the appropriate rule.
        continue;
      }

      $types[$type] = [];
      $bundlesForType = $bundleInfoService->getBundleInfo($type);

      /*
       * If an entity type doesn't have bundles then don't add anything.
       */
      if (!$this->entityTypeHasBundles($type)) {
        continue;
      }

      // Build an array where the bundleID is the index and the label the value.
      foreach ($bundlesForType as $bundleId => $bundleInfo) {
        $types[$type][$bundleId] = $bundleInfo['label'];
      }
    }

    // Generate the default_values arrays.
    $default_values = [];
    foreach ($value as $entityType => $bundles) {
      $default_values[$entityType] = [];
      foreach ($bundles as $bundle => $enabled) {
        if ($enabled) {
          $default_values[$entityType][] = $bundle;
        }
      }
    }

    $wrapper = [
      '#title' => \t('Select the bundles to enable'),
      '#type' => 'fieldset',
    ];

    $entityTypeManager = \Drupal::service('entity_type.manager');
    foreach ($types as $type => $bundles) {
      // Get entity type definition to get the appropriate label.
      $typeDefinition = $entityTypeManager->getDefinition($type);

      $wrapper[$type] = [
        '#type' => 'checkboxes',
        '#title' => $typeDefinition->getLabel(),
        '#options' => $bundles,
        '#default_value' => $default_values[$type],
      ];
    }

    return $wrapper;
  }

  /**
   * {@inheritDoc}
   */
  public function checkEntityAgainstConfig(array $value, EntityInterface $entity): bool {
    // Determine if this entity type has bundles.
    if ($this->entityTypeHasBundles($entity->getEntityTypeId())) {
      $type = $entity->getEntityTypeId();
      $bundle = $entity->bundle();
      // Verify that this entity is of a bundle type that is enabled.
      $value = !!$value[$type][$bundle];
      return $value;
    }

    // If the entity type doesn't have bundles defined then flag as passed.
    return TRUE;
  }

  /**
   * Determine if a given entity type has bundles defined.
   *
   * @param string $type
   *   Entity type ID.
   *
   * @return bool
   *   TRUE/FALSE flag identifying if the entity type has bundles defined.
   */
  protected function entityTypeHasBundles(string $type) {
    $bundlesForType = \Drupal::service('entity_type.bundle.info')->getBundleInfo($type);

    /*
     * If an entity type doesn't have bundles the array will be 1 item long
     * and its key will be the entity type ID.
     */
    if (count($bundlesForType) === 1 && isset($bundlesForType[$type])) {
      return FALSE;
    }

    return TRUE;
  }

}
