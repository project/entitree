<?php

namespace Drupal\entitree_location_rules\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Entitree location rule plugins.
 */
abstract class EntitreeLocationRuleBase extends PluginBase implements EntitreeLocationRuleInterface {
}
