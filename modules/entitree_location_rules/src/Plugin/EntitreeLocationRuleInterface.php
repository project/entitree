<?php

namespace Drupal\entitree_location_rules\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines an interface for Entitree location rule plugins.
 */
interface EntitreeLocationRuleInterface extends PluginInspectionInterface {

  /**
   * Generate the Form API to be used on the Entitree Ruleset Entity Form.
   *
   * @param array $value
   *   The current value for this rule.
   * @param array $rules
   *   Array of rule configurations defined for a ruleset.
   *
   * @return array
   *   Form API render array.
   */
  public function generateFormElement(array $value, array $rules): array;

  /**
   * Determine if the provided entity meets the configured rule.
   *
   * @param array $value
   *   Rule configuration associated with a ruleset.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Drupal entity to check against.
   *
   * @return bool
   *   TRUE or FALSE flag identifying if the entity passes the configured rules.
   */
  public function checkEntityAgainstConfig(array $value, EntityInterface $entity): bool;

}
