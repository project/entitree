<?php

namespace Drupal\entitree_location_rules\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Entitree location rule plugin manager.
 */
class EntitreeLocationRuleManager extends DefaultPluginManager {

  /**
   * Constructs a new EntitreeLocationRuleManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/EntitreeLocationRule', $namespaces, $module_handler, 'Drupal\entitree_location_rules\Plugin\EntitreeLocationRuleInterface', 'Drupal\entitree_location_rules\Annotation\EntitreeLocationRule');

    $this->alterInfo('entitree_location_rule_info');
    $this->setCacheBackend($cache_backend, 'entitree_location_rule_plugins');
  }

  /**
   * Get an instance of the rule type plugin.
   *
   * @param string $ruleType
   *   The rule type to load.
   *
   * @return \Drupal\entitree_location_rules\Plugin\EntitreeLocationRuleInterface
   *   An instance of the Entitree Location Rule plugin.
   */
  public function getEntitreeEntityType(string $ruleType): EntitreeLocationRuleInterface {
    return $this->createInstance($ruleType);
  }

}
