<?php

namespace Drupal\entitree_location_rules;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\entitree\EntitreeLocationInterface;

/**
 * Defines a class to build a listing of Entitree location ruleset entities.
 *
 * @ingroup entitree_location_rules
 */
class EntitreeLocationRulesetListBuilder extends EntityListBuilder {

  /**
   * @var array $locationRulePluginDefinitions
   */
  protected $locationRulePluginDefinitions = [];

  /**
   * {@inheritDoc}
   */
  protected function getTitle() {
    return $this->t('Entity location rulesets');
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Entitree location ruleset');
    $header['rules'] = $this->t('Rules');
    $header['destinations'] = $this->t('Destinations');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\entitree_location_rules\Entity\EntitreeLocationRuleset $entity */
    $row['label'] = $entity->label();
    /** @var \Drupal\entitree_location_rules\Entity\EntitreeLocationRulesetInterface $entity */
    $rules = $entity->getRules();

    $rulesMarkup = '<ul>';
    foreach ($rules as $rule) {
      if (!isset($this->locationRulePluginDefinitions[$rule['rule_type']])) {
        $this->locationRulePluginDefinitions[$rule['rule_type']] =
        \Drupal::service('plugin.manager.entitree_location_rule')
          ->getDefinition($rule['rule_type']);
      }

      $rulesMarkup .= '<li>' . $this->locationRulePluginDefinitions[$rule['rule_type']]['label'] . '</li>';
    }
    $rulesMarkup .= '</ul>';

    $row['rules'] = new FormattableMarkup($rulesMarkup, []);

    $destinationsMarkup = array_reduce($entity->getDestinations(), function (string $out,
      EntitreeLocationInterface $destination
    ) {
      $out .= '<div class="entitree_ruleset_destination">' . $destination->getLabel() . '</div>';
      return $out;
    }, ' ');
    $row['destinations'] = new FormattableMarkup($destinationsMarkup, []);

    return $row + parent::buildRow($entity);
  }

}
