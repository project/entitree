<?php

namespace Drupal\entitree_location_rules\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Entitree location ruleset entities.
 *
 * @ingroup entitree_location_rules
 */
class EntitreeLocationRulesetDeleteForm extends ContentEntityDeleteForm {


}
