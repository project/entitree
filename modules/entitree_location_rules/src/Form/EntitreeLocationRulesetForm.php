<?php

namespace Drupal\entitree_location_rules\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entitree\Entity\EntitreeLocation;
use Drupal\entitree_location_rules\Entity\EntitreeLocationRulesetInterface;

/**
 * Form controller for Entitree location ruleset edit forms.
 *
 * @ingroup entitree_location_rules
 */
class EntitreeLocationRulesetForm extends ContentEntityForm {

  /**
   * Storage of plugin instances of the EntitreeLocationRule type.
   *
   * @var array
   */
  protected $locationRulePluginInstances = [];

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    /** @var \Drupal\entitree_location_rules\Entity\EntitreeLocationRulesetInterface $entitree_location_ruleset_entity */
    $entitree_location_ruleset_entity = $this->entity;

    $storage = $form_state->getStorage();
    $storage['addMessages'] = isset($storage['addMessages']) ? $storage['addMessages'] : [];

    if (!isset($storage['current_rules'])) {
      $storage['current_rules'] = $entitree_location_ruleset_entity->getRules();

      /*
       * @todo this is here because the rules are not saving properly causing
       * the getter to return a null value. This should be removed.
       */
      if (!$storage['current_rules']) {
        $storage['current_rules'] = [];
      }
    }

    if (!isset($storage['destinations'])) {
      $storage['destinations'] = $entitree_location_ruleset_entity->getDestinations();
    }

    // Call method to determine appropriate AJAX request and process.
    if ($triggeringElement = $form_state->getTriggeringElement()) {
      $this->processAjax($triggeringElement, $storage, $form_state->getValues());
    }

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $entitree_location_ruleset_entity->label(),
      '#description' => $this->t("Label for the Entitree location ruleset entity."),
      '#required' => TRUE,
    ];

    /*
     * Add settings display
     */
    $form['settings'] = $this->generateSettingsDisplay($entitree_location_ruleset_entity);

    $form['rulesWrapper'] = $this->generateRulesList($storage['current_rules']);

    $form['rulesWrapper']['addRule'] = $this->generateAddFields($storage);

    /*
     * Add destination location display and selection
     */
    $form['destination'] = $this->generateDestinationDisplay($entitree_location_ruleset_entity, $storage);

    /*
     * Empty addMessages from storage so they are not repeated
     */
    $storage['addMessages'] = [];

    $form_state->setStorage($storage);

    // Add Entitree library to form to use tree browser styles.
    if (!isset($form['#attached'])) {
      $form['#attached'] = ['library' => []];
    }
    if (!isset($form['#attached']['library'])) {
      $form['#attached']['library'] = [];
    }
    $form['#attached']['library'][] = 'entitree/entitree';

    return $form;
  }

  /**
   * If the form is being rebuilt for an AJAX request, process as necessary.
   *
   * @param array $triggeringElement
   *   Form API element triggered by the user.
   * @param array $storage
   *   FORM API storage array.
   * @param array $values
   *   Current values of the form upon AJAX start.
   */
  protected function processAjax(array $triggeringElement, array &$storage, array $values) {
    /*
     * If this is an AJAX call to add a new rule add a rule of the appropriate
     * type here
     */
    if ($triggeringElement && $triggeringElement['#name'] === 'add_rule') {
      if (empty($values['addRule']['addRuleType'])) {
        $storage['addMessages'][] = $this->t('A rule type must be selected before adding it to the ruleset');
      }
      else {
        /*
         * @todo validate rule type selection
         */
        $storage['current_rules'][] = [
          'rule_type' => $values['addRule']['addRuleType'],
          'value' => [],
        ];
      }
    }

    /*
     * If this is an AJAX call to select a destination flag it in storage in
     * order to generate the structure display later
     */
    if ($triggeringElement && $triggeringElement['#name'] === 'select_destination') {
      $storage['showDestinationSelection'] = TRUE;
    }
    elseif ($triggeringElement && $triggeringElement['#name'] === 'cancel_select_destination') {
      $storage['showDestinationSelection'] = FALSE;
    }

    // If this is a browse to location request, set the new parent in storage.
    if (
      isset($triggeringElement['#data'])
      && isset($triggeringElement['#data']['method'])
      && $triggeringElement['#data']['method'] == 'browse'
    ) {
      $storage['destination_parent_id'] = $triggeringElement['#data']['location_id'];
    }

    // If this is a select location request, set the location in storage.
    if (
      isset($triggeringElement['#data'])
      && isset($triggeringElement['#data']['method'])
      && $triggeringElement['#data']['method'] == 'select'
    ) {
      // Check if this destination has already been selected.
      $matches = array_filter($storage['destinations'], function ($destination) use ($triggeringElement) {
        return $destination->id() == $triggeringElement['#data']['location_id'];
      });

      // If this destination hasn't already been selected then add it.
      if (!count($matches)) {
        $storage['destinations'][] = EntitreeLocation::load($triggeringElement['#data']['location_id']);
      }

      // Remove the browser parent ID to reset the form.
      unset($storage['destination_parent_id']);

      // Set show destination browser to false.
      $storage['showDestinationSelection'] = FALSE;
    }

    // If this is a delete destination request, remove location from storage.
    if (
      isset($triggeringElement['#data'])
      && isset($triggeringElement['#data']['method'])
      && $triggeringElement['#data']['method'] == 'delete'
    ) {
      $storage['destinations'] = array_filter($storage['destinations'], function ($destination) use ($triggeringElement) {
        return $destination->id() != (int) $triggeringElement['#data']['location_id'];
      });
    }
  }

  /**
   * Build form elements for list of defined rules.
   *
   * @param array $rules
   *   Array of rules configuration values..
   *
   * @return array
   *   Form API element representing the rules container.
   */
  protected function generateRulesList(array $rules): array {
    $rulesContainer = [
      '#type' => 'fieldset',
      '#title' => $this->t('Rules'),
      '#description' => $this->t('Add and configure the necessary rules. All rules must pass for a location to be created by this ruleset.'),
      '#prefix' => '<div id="rules-wrapper">',
      '#suffix' => '</div>',
    ];

    $tabs = [
      '#type' => 'vertical_tabs',
      '#tree' => TRUE,
    ];

    foreach ($rules as $index => $rule) {
      if (!isset($rule[''])) {
        $tabs['rule_' . $index . '_wrapper'] = $this->buildRuleConfigDisplay($rules, $index);
      }
    }

    $rulesContainer['rules'] = $tabs;

    return $rulesContainer;
  }

  /**
   * Build form elements for rule configuration.
   *
   * @param array $rules
   *   The array of enabled rules plugin configurations.
   * @param int $weight
   *   The order of this rule in the list of selected rules.
   *
   * @return array
   *   Form API array for this rule config.
   */
  protected function buildRuleConfigDisplay(array $rules, int $weight): array {
    $rule = $rules[$weight];

    if (!isset($this->locationRulePluginInstances[$rule['rule_type']])) {
      $this->locationRulePluginInstances[$rule['rule_type']] =
        \Drupal::service('plugin.manager.entitree_location_rule')
          ->createInstance($rule['rule_type']);
    }

    $instance = $this->locationRulePluginInstances[$rule['rule_type']];

    $wrapper = [
      '#type' => 'details',
      '#group' => 'rules',
      '#title' => $instance->getPluginDefinition()['label'],
    ];

    $wrapper['rule_' . $weight] = $instance->generateFormElement($rule['value'], $rules);
    $wrapper['#weight'] = $weight;
    return $wrapper;
  }

  /**
   * Given the form storage values determine what rule types can be added.
   *
   * @param array $storage
   *   Form API storage array.
   *
   * @return array
   *   Array where plugin ID is the key and the plugin definition is the value.
   */
  protected function getAvailableRuleTypes(array $storage): array {
    $currentRules = $storage['current_rules'];
    $allRuleTypes = \Drupal::service('plugin.manager.entitree_location_rule')->getDefinitions();

    $ruleTypes = [];

    foreach ($allRuleTypes as $plugin) {
      /*
       * If this plugin has the unique flag only add it as an option if it is
       * not already in use
       */
      if ($plugin['unique'] && $this->locationRuleTypeInUse($plugin['id'], $currentRules)) {
        continue;
      }

      // Filter out plugin if dependencies aren't met.
      if (isset($plugin['dependencies'])) {
        foreach ($plugin['dependencies'] as $dependency) {
          if (!$this->locationRuleTypeInUse($dependency, $currentRules)) {
            // One of the dependencies is not met. Skip this plugin.
            continue 2;
          }
        }
      }

      $ruleTypes[$plugin['id']] = $plugin;
    }

    return $ruleTypes;
  }

  /**
   * Build form elements to render add new rule button and related messages.
   *
   * @param array $storage
   *   Form API storage array.
   *
   * @return array
   *   Form API array of the add new rule button and any related messages.
   */
  protected function generateAddFields(array $storage): array {
    $addRule = [
      '#type' => 'details',
      '#title' => $this->t('Add new rule'),
      '#weight' => 100,
      '#tree' => TRUE,
    ];

    /*
     * If any messages have been set related to the add rule components render
     * them here
     */
    if (!empty($storage['addMessages']) && count($storage['addMessages']) > 0) {
      $addRule['messages'] = [
        '#prefix' => '<div class="messages">',
        '#markup' => '',
        '#suffix' => '</div>',
      ];
      foreach ($storage['addMessages'] as $message) {
        $addRule['messages']['#markup'] .= '<p class="message">' . $message . '</p>';
      }
    }

    $availableRuleTypes = $this->getAvailableRuleTypes($storage);

    $options = ['' => ''];
    foreach ($availableRuleTypes as $plugin) {
      $options[$plugin['id']] = $plugin['label'];
    }

    /*
     * @todo implement validation supression for required fields when
     * implemented in core
     * (https://www.drupal.org/project/drupal/issues/2476569)
     */
    $addRule['addButton'] = [
      '#type' => 'button',
      '#value' => $this->t('Add Rule'),
      '#name' => 'add_rule',
      '#weight' => 100,
      '#ajax' => [
        'callback' => '::handleAddRule',
        'disable-refocus' => FALSE,
        'wrapper' => 'rules-wrapper',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Adding new rule...'),
        ],
      ],
    ];

    /*
     * If no rules are available inform the user and disable the button
     */
    if (count($options) === 1) {
      $addRule['addRuleType'] = [
        '#markup' => '<p>' . $this->t('There no are rules available') . "</p>",
        '#weight' => 9,
      ];
      $addRule['addButton']['#attributes']['disabled'] = 'disabled';
    }
    else {
      $addRule['addRuleType'] = [
        '#type' => 'select',
        '#title' => $this->t('Select a rule type to add'),
        '#options' => $options,
        '#weight' => 9,
      ];
    }

    return $addRule;
  }

  /**
   * Determine if a specific rule type is in use by the associated entity.
   *
   * @param string $type
   *   Name of the rule plugin type.
   * @param array $rules
   *   Array containing names of EntitreeLocationRule plugins currently enabled.
   *
   * @return bool
   *   TRUE or FALSE flag identifying if specific plugin is in use.
   */
  protected function locationRuleTypeInUse(string $type, array $rules): bool {
    foreach ($rules as $rule) {
      if (!isset($rule['']) && $rule['rule_type'] === $type) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Called by add rule button to render additional rule config to the form.
   *
   * @param array $form
   *   Form API array of the form in its current state.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form API form state.
   */
  public function handleAddRule(array &$form, FormStateInterface &$form_state) {
    return $form['rulesWrapper'];
  }

  /**
   * Build Form API array for destination display.
   *
   * @return array
   *   Form API array
   */
  public function generateDestinationDisplay(EntitreeLocationRulesetInterface $entity, array $storage): array {
    $destinationWrapper = [
      '#type' => 'fieldset',
      '#title' => $this->t('Destinations'),
      '#prefix' => '<div id="destination-select-wrapper">',
      '#suffix' => '</div>',
    ];

    // Render list of currently selected destinations.
    if (count($storage['destinations'])) {
      $destinationWrapper['selectedDestinations'] = [
        '#prefix' => '<div class="ruleset_destinations_list">',
        '#markup' => '',
        '#suffix' => '</div>',
      ];
      foreach ($storage['destinations'] as $destination) {
        $destinationWrapper['selectedDestinations']['selectedDestination_' . $destination->id()] = [
          '#prefix' => '<div class="ruleset_destination">',
          '#markup' => $destination->getLabel(),
          '#suffix' => '</div>',
        ];

        // Add delete button for this destination.
        $destinationWrapper['selectedDestinations']['selectedDestination_' . $destination->id()]['selectedDestination_' . $destination->id() . '_delete'] = [
          '#type' => 'button',
          '#value' => 'D',
          '#name' => 'selected_destination_' . $destination->id(),
          '#ajax' => [
            'callback' => '::onEntitreeLocationSelect',
            'wrapper' => 'destination-select-wrapper',
          ],
          '#data' => [
            'location_id' => $destination->id(),
            'method' => 'delete',
          ],
        ];
      }
    }
    else {
      $destinationWrapper['currentDestinations'] = [
        '#markup' => $this->t('No destination locations have been selected'),
      ];
    }

    $showDestinationSelection = isset($storage['showDestinationSelection']) ? $storage['showDestinationSelection'] : FALSE;
    /*
     * If the Select Destination button has been clicked show the Entitree
     * structure. Otherwise show the Select Destination button.
     */
    if ($showDestinationSelection) {
      // Determine if parent has been selected via AJAX request.
      if (isset($storage['destination_parent_id'])) {
        $parent = EntitreeLocation::load($storage['destination_parent_id']);
      }

      // Render Form display of Entitree tree display.
      $destinationWrapper['locationSelect'] = \Drupal::service('entitree.manager')->generateLocationSelectionFormArray('destination-select-wrapper', $parent);

      // Render Cancel button.
      $destinationWrapper['cancelSelectDestination'] = [
        '#type' => 'button',
        '#prefix' => '<div>',
        '#value' => $this->t('Cancel'),
        '#suffix' => '</div>',
        '#name' => 'cancel_select_destination',
        '#ajax' => [
          'callback' => '::handleSelectDestination',
          'disable-refocus' => FALSE,
          'wrapper' => 'destination-select-wrapper',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Canceling...'),
          ],
        ],
      ];
    }
    else {
      /*
       * Select destination button
       */
      $destinationWrapper['selectDestination'] = [
        '#type' => 'button',
        '#prefix' => '<div>',
        '#value' => $this->t('Select Destination'),
        '#suffix' => '</div>',
        '#name' => 'select_destination',
        '#ajax' => [
          'callback' => '::handleSelectDestination',
          'disable-refocus' => FALSE,
          'wrapper' => 'destination-select-wrapper',
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Loading Entitree destinations...'),
          ],
        ],
      ];
    }

    return $destinationWrapper;
  }

  /**
   * Build Form API array for settings display.
   *
   * @param \Drupal\entitree_location_rules\Entity\EntitreeLocationRulesetInterface $entity
   *   Ruleset entity associated with this form.
   *
   * @return array
   *   Form API array for the settings fieldset.
   */
  public function generateSettingsDisplay(EntitreeLocationRulesetInterface $entity): array {
    $wrapper = [
      '#type' => 'fieldset',
      '#title' => $this->t('Settings'),
    ];

    // TODO determine types based on enabled entity types.
    $validTokenTypes = ['site', 'random', 'current-date'];
    $validTokenTypes = array_unique(array_merge($validTokenTypes, \Drupal::service('entitree.manager')->getAllSupportedTokenTypes()));

    $wrapper['location_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location Label Pattern'),
      '#default_value' => $entity->getLocationLabel(),
      '#description' => $this->t('Define the label of the Entitree location when one is created. This can be plain text or you can use tokens.'),
      '#element_validate' => ['token_element_validate'],
      '#token_types' => $validTokenTypes,
      '#required' => TRUE,
    ];

    $wrapper['location_path_segment'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location Path Segment Pattern'),
      '#default_value' => $entity->getLocationPathSegment(),
      '#description' => $this->t('Define the path segment pattern of the Entitree location when one is created. This can be plain text or you can use tokens.'),
      '#element_validate' => ['token_element_validate'],
      '#token_types' => $validTokenTypes,
      '#required' => TRUE,
    ];

    $wrapper['token_tree'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => $validTokenTypes,
      '#show_restricted' => TRUE,
      '#global_types' => FALSE,
    ];

    return $wrapper;
  }

  /**
   * Called when a location is selected to browse on the location select.
   *
   * @param array $form
   *   Form API array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form API state.
   *
   * @return array
   *   Form API array of wrapper element
   */
  public function onEntitreeLocationBrowse(array $form, FormStateInterface $form_state) {
    return $form['destination'];
  }

  /**
   * Called when a location is selected in the Entitree browser.
   *
   * @param array $form
   *   Form API array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *   Form API state.
   *
   * @return array
   *   Form API array of wrapper element
   */
  public function onEntitreeLocationSelect(array $form, FormStateInterface $form_state) {
    return $form['destination'];
  }

  /**
   * Handle Select Destination button click by returning the destination.
   *
   * @param array $form
   *   Form API form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form API form state.
   *
   * @return array
   *   Destination element of form
   */
  public function handleSelectDestination(array $form, FormStateInterface $form_state) {
    return $form['destination'];
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    /** @var \Drupal\entitree_location_rules\Entity\EntitreeLocationRulesetInterface $entitree_location_ruleset_entity */
    $entitree_location_ruleset_entity = $this->entity;

    $storage = $form_state->getStorage();
    $currentRules = $storage['current_rules'];

    $values = $form_state->getValues();

    /*
     * Set rules
     */
    $rules = [];
    /** @var \Drupal\entitree_location_rules\Plugin\EntitreeLocationRuleInterface $entityLocationRuleManager **/
    $entityLocationRuleManager = \Drupal::service('plugin.manager.entitree_location_rule');
    foreach ($values['rules'] as $name => $rule) {
      // The add rule fieldset is a child of the rules value so ignore it.
      if ($name === 'rules__active_tab') {
        continue;
      }

      list($prefix, $index) = explode('_', $name);

      $rules[] = [
        'rule_type' => $currentRules[$index]['rule_type'],
        'value' => $rule[array_keys($rule)[0]],
      ];
    }

    $entitree_location_ruleset_entity->setRules($rules);

    // Set the destination using the location ID.
    $entitree_location_ruleset_entity->set('destinations', array_map(function ($destination) {
      return (int) $destination->id();
    }, $storage['destinations']));

    $status = $entitree_location_ruleset_entity->save();

    switch ($status) {
      case SAVED_NEW:
        \Drupal::messenger()->addMessage($this->t('Created the %label Entitree location ruleset entity.', [
          '%label' => $entitree_location_ruleset_entity->label(),
        ]));
        break;

      default:
        \Drupal::messenger()->addMessage($this->t('Saved the %label Entitree location ruleset entity.', [
          '%label' => $entitree_location_ruleset_entity->label(),
        ]));
    }
    $form_state->setRedirectUrl($entitree_location_ruleset_entity->toUrl('collection'));
  }

}
